import { HomePage } from "../pages/HomePage";
import { StudentStatusQuery } from "../pages/StudentStatusQuery";
import { ArchiveIndex } from "../pages/archive";
import { Gdjy } from "../pages/archive/Content/Gdjy";
import { GdjyDetail } from "../pages/archive/Content/GdjyDetail";
import { GdjyDetailTwo } from "../pages/archive/Content/GdjyDetailTwo";
import { ReportBox } from "../pages/archive/Content/ReportBox";
import { ReportXjBox } from "../pages/archive/Content/ReportXjBox";
import { Scattered } from "../pages/archive/Content/Scattered";
import { Verify } from "../pages/Verify";
import { Query } from "../pages/Query";
import Login from "../pages/Login";

const route = [
	{ path: "/", name: "首页", components: HomePage },
	{ path: "/index", name: "首页", components: HomePage },
	{ path: "/statusQuery", name: "学籍查询", components: StudentStatusQuery },
	{ path: "/archiveIndex", name: "学籍查询", components: ArchiveIndex },
	{ path: "/login", name: "登录_学信档案", components: Login },
	{ path: "/active/gdjy", name: "高等教育", components: Gdjy },
	{ path: "/gdjy/detail", name: "高等教育学籍详情-1", components: GdjyDetail },
	{ path: "/gdjy/detail/two", name: "高等教育学籍详情-2", components: GdjyDetailTwo },
	{ path: "/gdjy/ReportBox", name: "学历报告", components: ReportBox },
	{ path: "/gdjy/ReportXjBox", name: "学历报告", components: ReportXjBox },
	{ path: "/Verify", name: "查询", components: Verify },
	{ path: "/Query", name: "验证", components: Query },
	{ path: "/Scattered", name: "零散查询", components: Scattered },
	// { path: "/login", name: "登录_学信档案", components: Login },
];

export const routers = [...route];
