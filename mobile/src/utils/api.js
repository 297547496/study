import req from './request'

export function getData(url, method, data) {
    console.log(url, method, data);
    return req({
        // `url` 是用于请求的服务器 URL
        url: url,

        // `method` 是创建请求时使用的方法
        method: method, // 默认是 get

        //需要传递到服务器的 参数
        data
    })
}
