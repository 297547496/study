import { Routes, Route } from "react-router-dom";
import { routers } from "./router";
import "./App.css";

function App() {
	return (
		<div className="App App-header">
			<Routes className="App App-content">
				{routers.map((item, index) => (
					<Route path={item.path} key={index} element={<item.components />}></Route>
				))}
			</Routes>
		</div>
	);
}

export default App;
