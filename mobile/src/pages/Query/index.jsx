
import "./index.css"
import { CloseOutline, UnorderedListOutline } from 'antd-mobile-icons'
import { useState } from 'react'
import { Swiper, Popup } from 'antd-mobile'
import { NAV_ITEMS } from '../HomePage/consts'
import { Footer } from '../Footer'

export const Query = () => {
    const [isShowPopup, setIsShowPopup] = useState(false)
    const [username, setUsername] = useState('');

    // 导航
    const navContent = () => {
        return (
            <div className='my-nav'>
                {NAV_ITEMS.map(item => {
                    return (<div className='nav-items'>
                        {item.map(e => {
                            return (
                                <div className="nav-items-item" key={e.key}>
                                    <a href={e.hrefUrl}>{e.text}</a>
                                </div>
                            )
                        })}
                    </div>)
                })}</div>
        )
    }
    const handleLogin = async () => {
        if (username.target.value == '') {
            return;
        }
        window.open('/gdjy/ReportBox?id=' + username.target.value)
    }

    return (
        <div class="outer-wrap">
            <div id="app" class="outer-content">
                {/* <div class="wap-nav-bar no-border">
                    <div class="wap-nav-bar__left">
                        <a href="/">
                            <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" class="logo" />
                        </a>
                    </div>
                    <div class="wap-nav-bar__center">
                    </div>
                    <div class="wap-nav-bar__right">
                        <span class="ch-nav">
                            <i class="ch-icon ch-icon-wap-nav">
                            </i>
                        </span>
                    </div>
                </div> */}
                <div className="wap-nav-bar no-border">
                    <div className="wap-nav-bar__left"><a href="/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="logo" /></a>
                    </div>
                    <div className="wap-nav-bar__right" onClick={() => setIsShowPopup(true)}>
                        <span className="ch-nav"><UnorderedListOutline /></span>
                    </div>
                </div>

                <h2 class="h2-banner" style={{ color: '#333' }}>学籍/学历/学位在线验证报告查询
                </h2> <div class="seach-wrap"><form method="post" name="getXueLi" action="/xlcx/bg.do?vcode="><div class="van-cell-group van-cell-group--inset van-hairline--top-bottom"><div class="van-cell van-field"><div class="van-cell__title van-field__label"><span>在线验证码
                </span>
                </div><div class="van-cell__value van-field__value"><div class="van-field__body">


                    <input value={username.value} onChange={(value) => setUsername(value)} type="text" autocomplete="off" name="validationCode" placeholder="请输入报告中的在线验证码" class="van-field__control" />


                </div>
                    </div>
                </div>
                </div>
                    <button type="button" class="serch-btn van-button van-button--primary van-button--normal van-button--block" onClick={handleLogin}><div class="van-button__content"><span class="van-button__text">查询
                    </span>
                    </div>
                    </button>
                </form> <div class="tip-text">2019年3月15日起，新申请的《教育部学籍在线验证报告》及《教育部学历证书电子注册备案表》的在线验证码升级为16位。
                    </div> <div class="tip-text">2023年1月16日起，启用新版《教育部学籍在线验证报告》及《教育部学历证书电子注册备案表》，点此查看<a href="/xlcx/ybdb.jsp" target="_blank" class="green-link">新旧版报告样式对比
                    </a>。
                    </div> <ul class="queryid-tips"><li class="tip-title">提供的验证
                    </li> <li class="tip-text">1.《教育部学籍在线验证报告》（含中文版和翻译件（英文））
                        </li> <li class="tip-text">2.《教育部学历证书电子注册备案表》（含中文版和翻译件（英文））
                        </li> <li class="tip-text">3.《中国高等教育学位在线验证报告》（含中文版和翻译件（英文））
                        </li>
                    </ul> <div class="van-row other-link-wrap"><div class="van-col van-col--8"><a href="/xlcx/bgys.jsp">验证报告简介
                    </a>
                    </div><div class="van-col van-col--8"><a href="/xlcx/fwcs.jsp">防伪措施
                    </a>
                        </div><div class="van-col van-col--8"><a href="/xlcx/yzzw.jsp">验证真伪
                        </a>
                        </div><div class="van-col van-col--8"><a href="/xlcx/tdhyt.jsp">特点和用途
                        </a>
                        </div><div class="van-col van-col--8"><a href="/xlcx/rhsq.jsp">如何申请
                        </a>
                        </div><div class="van-col van-col--8"><a href="/xlcx/rhsy.jsp">如何使用
                        </a>
                        </div><div class="van-col van-col--8"><a href="/xlcx/rhyq.jsp">延长验证有效期
                        </a>
                        </div><div class="van-col van-col--8"><a href="/xlcx/tbsm.jsp">特别声明
                        </a>
                        </div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
            {/* 导航弹出层 */}
            {isShowPopup && (<Popup
                visible={isShowPopup}
                onMaskClick={() => {
                    setIsShowPopup(false)
                }}
                onClose={() => {
                    setIsShowPopup(false)
                }}
                position='top'
                bodyStyle={{ height: '' }}
            >
                <div className="my-close-bar">
                    <a href="/wap/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="my-logo" />
                    </a>
                    <CloseOutline onClick={() => setIsShowPopup(false)} />
                </div>
                {navContent()}
            </Popup>)}
        </div>

    )
}