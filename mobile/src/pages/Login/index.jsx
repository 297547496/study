
import React, { useState } from 'react';
import { Input, Button, Toast } from 'antd-mobile';
import { getData } from '../../utils/api';
import './index.css'

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    if (!username || !password) {
      Toast.fail('请输入用户名和密码');
      return;
    }
    const res = await getData('/api/user/login', 'post', { account: username, password })
    if (res.data.code == 1) {
      window.localStorage.setItem('token', res.data.data.userinfo.token)
      Toast.show({
        icon: 'success',
        content: '登录成功',
      })
      window.location.href = '/archiveIndex'
      // window.open('/archiveIndex')
    }else{
      alert(res.data.msg)
    }
  }

  return (
    <div className='login-page'>
      <header className='login-header'>
      </header>
      <p>请使用<a style={{ color: '#1887E0' }} href='https://account.chsi.com.cn/account/help/index.jsp?keywords=%E5%AD%A6%E4%BF%A1%E7%BD%91%E8%B4%A6%E5%8F%B7%E5%8F%AF%E4%BB%A5%E5%81%9A%E4%BB%80%E4%B9%88'> 学信网账号 </a>进行登录</p>
      <Input
        placeholder="手机号/邮箱"
        value={username}
        onChange={(value) => setUsername(value)}
      />
      <Input
        type="password"
        placeholder="密码"
        value={password}
        onChange={(value) => setPassword(value)}
      />
      <Button type="primary" onClick={handleLogin} color='success'>
        登录
      </Button>
      <p className='forget-password'>
        <a href='https://account.chsi.com.cn/account/password!retrive'>找回密码</a>
        <a href='https://account.chsi.com.cn/account/preregister.action?from=archive'>注册</a>
      </p>
      <footer className='login-footer'>
        <div className='footer-text'>
          <a className='text' href='https://my.chsi.com.cn/archive/index.jsp'>
            <span>首页</span>|</a>
          <a className='text' href='https://my.chsi.com.cn/archive/help/index.action'><span>帮助中心</span>|</a>
          <a className='text' href="https://www.chsi.com.cn/about/contact.shtml"><span>联系我们</span>|</a>
          <a className='text' href="//kl.chsi.com.cn/robot/index.action?system=account"><span>学信机器人</span></a>
        </div>
        <div className="login-footer-text">
          Copyright © 2003-2023
          <a href="https://www.chsi.com.cn/" target="_blank" className='text-name'>学信网</a>
          All Rights Reserved
        </div>
      </footer>
    </div>
  );
};

export default Login;