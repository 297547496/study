
import "./index.css"
import { CloseOutline, UnorderedListOutline } from 'antd-mobile-icons'
import { useState } from 'react'
import { Swiper, Popup } from 'antd-mobile'
import { NAV_ITEMS } from '../HomePage/consts'
import { Footers } from '../Footers'

export const Verify = () => {
    const [isShowPopup, setIsShowPopup] = useState(false)
    // 导航
    const navContent = () => {
        return (
            <div className='my-nav'>
                {NAV_ITEMS.map(item => {
                    return (<div className='nav-items'>
                        {item.map(e => {
                            return (
                                <div className="nav-items-item" key={e.key}>
                                    <a href={e.hrefUrl}>{e.text}</a>
                                </div>
                            )
                        })}
                    </div>)
                })}</div>
        )
    }

    return (
        <div className="verifyBox">
            <div id="app" className="outer-content">
                <div className="wap-nav-bar no-border">
                    <div className="wap-nav-bar__left"><a href="/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="logo" /></a>
                    </div>
                    <div className="wap-nav-bar__right" onClick={() => setIsShowPopup(true)}>
                        <span className="ch-nav"><UnorderedListOutline /></span>
                    </div>
                </div>
            </div> <div className="lscx-banner"><h2 style={{ color: '#000' }}>中国高等教育学历证书查询
            </h2> <p><a href="https://www.chsi.com.cn/help/cxfw.jsp" target="_blank">学历查询范围 &gt;
            </a>
                </p>
            </div> <div className="lscx-main"><div className="lscx-item van-row van-row--flex"><div className="icons brcx-icon van-col">
            </div> <div className="lscx-des van-hairline--bottom van-col"><div><h3><a href="/statusQuery" target="_blank">本人查询
            </a>
            </h3> <a href="/xlcx/brcxff.jsp" target="_blank" className="lscx-entry">查询方法 &gt;
                </a>
            </div> <p>
                        注册学信网，登录学信档案，即可查询本人学历

                    </p>
                </div>
            </div> <div className="lscx-item van-row van-row--flex"><div className="icons lscx-icon van-col">
            </div> <div className="lscx-des van-hairline--bottom van-col"><div><h3><a href="/Scattered" target="_self">零散查询
            </a>
            </h3>
            </div> <p>
                            输入证书编号及姓名进行查询

                        </p>
                    </div>
                </div> <div className="lscx-item van-row van-row--flex"><div className="icons qycx-icon van-col">
                </div> <div className="lscx-des padding0 van-col"><div><h3><a href="https://jy.chsi.com.cn/corpusr/index.do" target="_blank">企业用户查询
                </a>
                </h3>
                </div> <p>企业用户登录国家大学生就业服务平台进行查询
                        </p>
                    </div>
                </div> <div className="lscx-report-des"><div className="report-img">
                </div> <h3>学历证书电子注册备案表
                    </h3> <p className="enter"><button className="report-button van-button van-button--default van-button--small van-button--plain van-button--round" style={{ color: 'rgb(163, 177, 191)', borderColor: 'rgb(163, 177, 191)' }}><div className="van-button__content"><span className="van-button__text">报告介绍
                    </span>
                    </div>
                    </button> <button className="report-button van-button van-button--default van-button--small van-button--plain van-button--round" style={{ color: 'rgb(163, 177, 191)', borderColor: 'rgb(163, 177, 191)' }}><div className="van-button__content"><span className="van-button__text">如何申请
                    </span>
                    </div>
                        </button>
                    </p>
                </div> <div className="lscx-report-des xlrz_rk"><div className="report-img">
                </div> <h3>中国高等教育学历认证报告
                    </h3> <p className="enter"><button className="report-button van-button van-button--default van-button--small van-button--plain van-button--round" style={{ color: 'rgb(163, 177, 191)', borderColor: 'rgb(163, 177, 191)' }}><div className="van-button__content"><span className="van-button__text">网上申请
                    </span>
                    </div>
                    </button> <button className="report-button van-button van-button--default van-button--small van-button--plain van-button--round" style={{ color: 'rgb(163, 177, 191)', borderColor: 'rgb(163, 177, 191)' }}><div className="van-button__content"><span className="van-button__text">查询
                    </span>
                    </div>
                        </button>
                    </p>
                </div>
            </div> <div className="van-overlay" style={{ display: 'none' }}>
                <div className="link-pop-wrapper">
                    < div className="link-pop-cxt">
                        <div className="xjxl-link-pop gd-link-pop">
                            <div className="list-wrap"><div className="sub-tit">请选择以下身份证件进行网上申请
                            </div>
                                <div className="van-row">
                                    <div className="van-col van-col--16">
                                        <a href="https://xlrz.chsi.com.cn/wssq/">居民身份证入口</a>
                                    </div> <div className="van-col van-col--16"><a href="https://www.chsi.com.cn/wssq/">其他身份证件入口
                                    </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            </div >
            <div style={{ position: 'fixed', bottom: '0', width: '100vw' }}>
                <Footers />
            </div>
            {/* 导航弹出层 */}
            {isShowPopup && (<Popup
                visible={isShowPopup}
                onMaskClick={() => {
                    setIsShowPopup(false)
                }}
                onClose={() => {
                    setIsShowPopup(false)
                }}
                position='top'
                bodyStyle={{ height: '' }}
            >
                <div className="my-close-bar">
                    <a href="/wap/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="my-logo" />
                    </a>
                    <CloseOutline onClick={() => setIsShowPopup(false)} />
                </div>
                {navContent()}
            </Popup>)}
        </div >

    )
}