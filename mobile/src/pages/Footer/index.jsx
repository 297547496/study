import './index.css'

export const Footer = () => {
  return (
    <div className="wap-footer">
      <div className="wap-footer-list">
        <div className="footer-a-wrap">
          <a href="https://chesicc.chsi.com.cn/zxgw/zxjs/201604/20160418/1529506207.html">中心简介</a>
        </div> <div className="footer-a-wrap">
          <a href="/about/about_site.shtml">网站简介</a></div>
        <div className="footer-a-wrap"><a href="/about/copyright.shtml">版权声明</a></div>
        <div className="footer-a-wrap"><a href="/ad/index.shtml">网站宣传</a></div>
        <div className="footer-a-wrap"><a href="/about/contact.shtml">联系我们</a>
        </div> <div className="footer-a-wrap">
          <a href="/zhaopin/index.jsp">招聘信息</a></div>
        <div className="footer-a-wrap"><a href="/help">帮助中心</a></div>
        <div className="footer-a-wrap"><a href="/about/ct16.shtml">大事记</a></div>
        <div className="footer-a-wrap"><a href="/z/tenyears/index.jsp">学信十周年</a></div></div>
      <div className="wap-footer-units"><div className="list">
        客服热线：010-67410388<br />
        客服邮箱：kefu#chsi.com.cn（将#替换为@）
      </div>
        <div className="list">
          主办单位：
          <a href="https://chesicc.chsi.com.cn/">教育部学生服务与素质发展中心</a><br />
          Copyright © 2003-2023
          <a href="https://www.chsi.com.cn/" target="_blank">学信网</a>
          All Rights Reserved<br /></div>
        <div className="list"><a href="https://beian.miit.gov.cn" target="_blank">京ICP备19004913号-1</a><br /> <a href="https://www.beian.gov.cn/portal/registerSystemInfo?recordcode=11010202009747" target="_blank">京公网安备11010202009747号</a></div></div></div>
  )
}