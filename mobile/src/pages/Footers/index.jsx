import './index.css'

export const Footers = () => {
  return (
    <div className="wap-footer wap-footer2 newfperm" >
      主办单位：<a href="https://chesicc.chsi.com.cn/">教育部学生服务与素质发展中心</a><br/>
        Copyright © 2003-2023 <a href="https://www.chsi.com.cn/" target="_blank">学信网</a> All Rights Reserved
    </div>
  )
}