import './index.css'
import {SWIPER, PART_FOUR_ITEMS, MEDIAS, NAV_ITEMS} from './consts'
import { Swiper,Popup } from 'antd-mobile'
import { CloseOutline, UnorderedListOutline } from 'antd-mobile-icons'
import { Footer } from '../Footer'
import { useState } from 'react'

export const StudentStatusQuery = () => {
  const [isShowPopup, setIsShowPopup] = useState(false)

  // 轮播渲染
  const renderSwiperItems = () => {
    return SWIPER.map(item =>  {
      return(
      <Swiper.Item key={item.key}>
        <a href={item.goUrl}>
          <img src={item.imgUrl} className='swiper-img'/>
        </a>
      </Swiper.Item>)
    }
    )
  }

  const renderPart4Block = () => {
    return(
      PART_FOUR_ITEMS.map(item => {
        return(
        <div className="van-grid" key={Math.random()}>
          {item.map(e => {
            return(
              <div className="van-grid-item" key={e.key}>
              <div className="van-grid-item__content van-grid-item__content--center">
                <a href={e.hrefUrl}>
                  <div className={`icon bgUrl${e.key}`}>
                  </div> 
                  <div className="text">{e.text}</div>
                </a>
              </div>
            </div>
            )})}
        </div>
        )
      })
    )
  }

  const renderMedia = () => {
   return <div className='van-grid'>
    {MEDIAS.map(item => {
      return(
        <div className="van-grid-item" ket={item.key}>
          <div className="van-grid-item__content van-grid-item__content--horizontal van-grid-item__content--center">
            <div className="van-grid-item__icon-wrapper">
              <img src={item.imgUrl} />
            </div>
            <span className="van-grid-item__text">{item.text}</span>
          </div>
        </div>
      )
    })}
    </div>
  }

  // 导航
  const navContent = () => {
    return(
      <div className='my-nav'>
      {NAV_ITEMS.map(item => {
         return( <div className='nav-items'>
          {item.map(e => {
            return (
              <div className="nav-items-item" key={e.key}>
            <a href={e.hrefUrl}>{e.text}</a>
          </div>
            )
          })}
       </div> )
      })}</div>
    )
  }


  return(
    <div>
      <div className="wap-nav-bar no-border">
        {/* 头部 */}
        <div className="wap-nav-bar__left"><a href="/">
          <img src="https://t2.chei.com.cn/archive/mobile/images/logo-white.png" alt="学信档案" className="logo" /></a>
        </div> 
        <div className="wap-nav-bar__right" onClick={() => setIsShowPopup(true)}>
          <span className="ch-nav"><UnorderedListOutline /></span>
        </div>
      </div>
      {/* 第二部分 */}
      <div className="part part1">
        <div className="text"></div> 
        <div className="btn-part van-row">
          <div className="van-col van-col--12">
            <a href="/login">
              <button className="my-btn van-button van-button--primary van-button--normal">
                <span className="van-button__text">登录</span>
              </button>
            </a>
          </div> 
          <div className="van-col van-col--12">
            <a href="https://account.chsi.com.cn/account/preregister.action?from=archive">
              <button className="my-btn van-button van-button--primary van-button--normal van-button--plain">
                <span className="van-button__text">注册学信网账号</span>
              </button>
            </a>
          </div>
        </div> 
        <div className="img"></div>
      </div>
      {/* 第三部分 */}
      <div id="gdjy" className="part part-white">
        <h2>免费查看学籍、学历、学位信息</h2> 
        <div className="part-img part2-img"></div>
        <p className="part-text">
          实名注册后即可免费查看本人的高等教育学籍、学历、学位、考研等信息。
        </p>
      </div>
      {/* 第四部分 */}
      <div id="txjd" className="part part-grey">
        <h2>校对本人学历照片</h2> 
        <div className="part-img part3-img"></div> 
        <p className="part-text">学历照片很重要，及时校对很有必要。</p>
      </div>
      {/* 第五部分 */}
      <div id="zxyz" className="part part-white">
        <h2>免费申请学籍、学历、学位<br />在线验证报告</h2> 
        <div className="part-img part4-img"></div> 
        <ul className="text-list">
          <li className="blue">
                    报告使用场景：
            <p>求职招聘、派遣接收、升学(考研、专升本)、出国留学、干部任免、职称评定、信用评估等。</p></li> <li className="yellow"> 报告特点：
            <p>多次免费验证、多重防伪、多次打印。</p>
          </li>
        </ul>
      </div>
      {/* 第六部分 */}
      <div id="xlycj" className="part part-grey">
        <h2>查看或绑定学历学位认证与成绩<br />验证报告</h2> 
        <div className="part-img part5-img"></div> 
        <ul className="text-list">
          <li className="blue">《中国高等教育学历认证报告》</li> 
          <li className="blue2">《中国中等教育学历验证报告》</li> 
          <li className="yellow">《中国高等学校学生成绩验证报告》</li> 
          <li className="orange">《中国高等学校招生入学考试成绩验证报告》 </li> 
          <li className="green">《中国中等学校学生成绩验证报告》</li> 
          <li>《普通高中学业水平考试（会考）成绩验证报告》</li> 
          <li className="purple">《中国高等教育学位认证报告》</li>
        </ul>
      </div>
      {/* 第七部分 */}
      <div id="hzsq" className="part part-white">
        <h2>向所申请的国外大学发送报告</h2> 
        <div className="part-img part6-img"></div> 
        <p className="ul-tit">合作机构：</p> 
        <ul className="text-list">
          <li className="blue">美国大学网ApplyWeb</li> 
          <li className="blue2">美国Parchment</li> 
          <li className="yellow">美国学生信息中心NSC</li> 
          <li className="orange">爱尔兰Digitary</li>
          <li className="green">荷兰DUO</li> 
          <li>World Education Services (WES)</li>
        </ul>
      </div>
      {/* 第八部分 */}
      <div id="dctp" className="part part-grey">
        <h2>参与调查问卷和投票</h2> 
        <div className="part-img part7-img"></div> 
        <ul className="text-list">
          <li className="blue">对所读大学的优势专业进行投票推荐</li> 
          <li className="blue2">对就读大学和就读专业进行满意度的评价</li> 
          <li className="yellow">对就业状况进行反馈</li>
        </ul>
      </div>
      {/* 第九部分 */}
      <div id="ncss" className="part part-white">
        <h2>享受教育部大学生职业网<br /> （新职业）的求职招聘服务</h2> 
        <div className="part-img part8-img"></div> 
        <ul className="text-list">
          <li className="blue">更真实的“职位库”，为你量身推荐，与众不同、独一无二 </li> 
          <li className="blue2">更优质的“企业库”，由你徜徉梦想天空，官方独有标签加持 </li> 
          <li className="yellow">更大型的“招聘会”，为你提供最多沟通机会，多部委、省市、高校联办 </li>
          <li className="orange">更独家的“师兄师姐去哪儿”，揭秘同学校同专业前辈们的就业方向 </li> 
          <li className="green">更官方的“推荐指数”，为你评估与心仪企业的匹配度</li> 
          <li>更接地气的“面试经验”，真实反映心仪职位的面试要点与实地体验</li>
        </ul>
      </div>
      {/* 最后一部分 */}
      <div className="part part1 part-last">
        <h3>现在就开始使用学信档案</h3> 
        <div className="btn-part van-row">
          <div className="van-col van-col--12">
            <a href="/archive/index.action">
              <button className="my-btn van-button van-button--primary van-button--normal">
                <span className="van-button__text">登录</span>
              </button>
            </a>
          </div> 
          <div className="van-col van-col--12">
            <a href="https://account.chsi.com.cn/account/preregister.action?from=archive">
              <button className="my-btn van-button van-button--primary van-button--normal van-button--plain">
                <span className="van-button__text">注册学信网账号</span>
              </button>
            </a>
          </div>
        </div>
      </div>
     
      {/* 底部 */}
      <div className="footer">
        <a href="http://www.chsi.com.cn/" target="_blank">学信网</a>　|　
        <a href="/archive/help/index.action" target="_blank">帮助中心</a>
        <p>Copyright © 2003-2023 学信网 All Rights Reserved</p>
      </div>
      {/* 导航弹出层 */}
      {isShowPopup && (<Popup
              visible={isShowPopup}
              onMaskClick={() => {
                setIsShowPopup(false)
              }}
              onClose={() => {
                setIsShowPopup(false)
              }}
              position='top'
              bodyStyle={{ height: '' }}
            >
              <div className="my-close-bar">
                <a href="/wap/">
                  <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="my-logo" />
                </a> 
                <CloseOutline onClick={() => setIsShowPopup(false)} />
              </div>
              {navContent()}
            </Popup>)}
    </div>
  )
}

