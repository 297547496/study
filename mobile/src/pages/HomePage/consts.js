export const SWIPER = [
	{
		goUrl: "https://gaokao.chsi.com.cn/gkxx/zt/2023gkfzp.shtml",
		imgUrl: "https://t3.chei.com.cn/news/img/2293097842.jpg",
		key: 1,
	},
	{
		goUrl: "https://gaokao.chsi.com.cn/gkxx/zt/xszzzc.shtml",
		imgUrl: "https://t4.chei.com.cn/news/img/2199051959.jpg",
		key: 2,
	},
	{
		goUrl: "https://xz.chsi.com.cn/xz/zt/jqzynl.shtml",
		imgUrl: "	https://t4.chei.com.cn/news/img/2292970680.jpg",
		key: 3,
	},
];

export const MEDIAS = [
	{
		imgUrl: "https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-wx.svg",
		key: 1,
		text: "学信网",
	},
	{
		imgUrl: "https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-wx.svg",
		key: 2,
		text: "学信网咨询",
	},
	{
		imgUrl: "https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-wx.svg",
		key: 3,
		text: "阳光高考",
	},
	{
		imgUrl: "https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-wx.svg",
		key: 4,
		text: "研招网",
	},
	{
		imgUrl: "https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-wx.svg",
		key: 5,
		text: "国家大学生就业服务平台",
	},
	{
		imgUrl: "https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-wx.svg",
		key: 6,
		text: "学职平台",
	},
];

export const PART_FOUR_ITEMS = [
	[
		{
			hrefUrl: "https://bm.chsi.com.cn/yszp/stu/judge",
			key: 1,
			text: "在线考试系统",
		},
		{
			hrefUrl: "https://bm.chsi.com.cn/ycms/stu/",
			key: 2,
			text: "远程面试系统",
		},
		{
			hrefUrl: "https://chsi.wanfangtech.net/",
			key: 3,
			text: "毕业生论文查重",
		},
	],
	[
		{
			hrefUrl: "https://www.chsi.com.cn/cjdyz/index",
			key: 4,
			text: "电子成绩单验证",
		},
		{
			hrefUrl: "https://jybzp.chsi.com.cn/home/index",
			key: 5,
			text: "教育部人才服务网",
		},
		{
			hrefUrl: "https://exwzs.chsi.com.cn/",
			key: 6,
			text: "第二学士学位招生",
		},
	],

	[
		{
			hrefUrl: "https://bm.chsi.com.cn/yszp/stu/judge",
			key: 7,
			text: "在线考试系统",
		},
		{
			hrefUrl: "https://bm.chsi.com.cn/yszp/stu/judge",
			key: 8,
			text: "在线考试系统",
		},
	],
];

export const NAV_ITEMS = [
	[
		{
			hrefUrl: "/",
			key: 1,
			text: "首页",
		},
		{
			hrefUrl: "/statusQuery",
			key: 2,
			text: "学籍查询",
		},
		{
			hrefUrl: "https://www.chsi.com.cn/xlcx/index.jsp",
			key: 3,
			text: "学历查询",
		},
	],
	[
		{
			hrefUrl: "https://www.chsi.com.cn/xwcx/index.jsp",
			key: 4,
			text: "学位查询",
		},
		{
			hrefUrl: "https://www.chsi.com.cn/xlcx/bgcx.jsp",
			key: 5,
			text: "在线验证",
		},
		{
			hrefUrl: "https://www.chsi.com.cn/xlrz/index2.jsp",
			key: 6,
			text: "出国教育背景...",
		},
	],

	[
		{
			hrefUrl: "https://my.chsi.com.cn/archive/index.jsp#txjd",
			key: 7,
			text: "图像校对",
		},
		{
			hrefUrl: "https://my.chsi.com.cn/archive/index.jsp",
			key: 8,
			text: "学信档案",
		},
    {
			hrefUrl: "https://gaokao.chsi.com.cn/",
			key: 9,
			text: "高考",
		},
	],
  [
		{
			hrefUrl: "https://yz.chsi.com.cn/",
			key: 10,
			text: "研招",
		},
		{
			hrefUrl: "https://www.gatzs.com.cn/",
			key: 11,
			text: "港澳台招生",
		},
    {
			hrefUrl: "https://www.gfbzb.gov.cn/",
			key: 12,
			text: "征兵",
		},
	],
  [
		{
			hrefUrl: "https://www.ncss.cn/",
			key: 13,
			text: "就业",
		},
		{
			hrefUrl: "https://xz.chsi.com.cn/home.action",
			key: 14,
			text: "学职平台",
		},
	],
];
