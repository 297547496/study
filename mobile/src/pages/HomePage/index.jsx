import './index.css'
import './index2.css'
import { SWIPER, PART_FOUR_ITEMS, MEDIAS, NAV_ITEMS } from './consts'
import { Swiper, Popup } from 'antd-mobile'
import { CloseOutline, UnorderedListOutline } from 'antd-mobile-icons'
import { Footer } from '../Footer'
import { useState } from 'react'
import xx from '../../images/x.png';

export const HomePage = () => {
  const [isShowPopup, setIsShowPopup] = useState(false)
  const [show, setShow] = useState(false)
  // if (window.navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)) {
  //   window.location = "http://h5.cxl1.cn/";

  // } else {
  //   window.location = "http://www.cxl1.cn/";
  // }
  // 轮播渲染
  const renderSwiperItems = () => {
    return SWIPER.map(item => {
      return (
        <Swiper.Item key={item.key}>
          <a href={item.goUrl}>
            <img src={item.imgUrl} className='swiper-img' />
          </a>
        </Swiper.Item>)
    }
    )
  }

  const renderPart4Block = () => {
    return (
      PART_FOUR_ITEMS.map(item => {
        return (
          <div className="van-grid" key={Math.random()}>
            {item.map(e => {
              return (
                <div className="van-grid-item" key={e.key}>
                  <div className="van-grid-item__content van-grid-item__content--center">
                    <a href={e.hrefUrl}>
                      <div className={`icon bgUrl${e.key}`}>
                      </div>
                      <div className="text">{e.text}</div>
                    </a>
                  </div>
                </div>
              )
            })}
          </div>
        )
      })
    )
  }

  const renderMedia = () => {
    return <div className='van-grid'>
      {MEDIAS.map(item => {
        return (
          <div className="van-grid-item" ket={item.key}>
            <div className="van-grid-item__content van-grid-item__content--horizontal van-grid-item__content--center">
              <div className="van-grid-item__icon-wrapper">
                <img src={item.imgUrl} />
              </div>
              <span className="van-grid-item__text">{item.text}</span>
            </div>
          </div>
        )
      })}
    </div>
  }

  // 导航
  const navContent = () => {
    return (
      <div className='my-nav'>
        {NAV_ITEMS.map(item => {
          return (<div className='nav-items'>
            {item.map(e => {
              return (
                <div className="nav-items-item" key={e.key}>
                  <a href={e.hrefUrl}>{e.text}</a>
                </div>
              )
            })}
          </div>)
        })}</div>
    )
  }


  return (
    <div>
      <div className="wap-nav-bar no-border">
        {/* 头部 */}
        <div className="wap-nav-bar__left"><a href="/">
          <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="logo" /></a>
        </div>
        <div className="wap-nav-bar__right" onClick={() => setIsShowPopup(true)}>
          <span className="ch-nav"><UnorderedListOutline /></span>
        </div>
      </div>
      {/* 第二部分 */}
      <div className="index-part1">
        <div className='swiper'>
          {/* {renderSwiperItems()} */}
          <Swiper autoplay>{renderSwiperItems()}</Swiper>
        </div>
        <div className="news-part">
          <div className="news-top-list">
            <div className="font16">
              <a href="https://yz.chsi.com.cn/kyzx/jybzc/202308/20230823/2293107421.html" target="_blank" className="colorRed">2024年教育专业学位硕士《教育综合考试大纲》正式公布</a>
            </div>
            <a href="https://yz.chsi.com.cn/kyzx/zt/lnfsx2023.shtml" target="_blank">近五年考研分数线及趋势图（2019-2023）</a>
            {'\u00A0'}|{'\u00A0'}
            <a href="https://www.ncss.cn/student/jobfair/fairdetails.html?fairId=YTwByZQmNyp8HWmcHCpYb1" target="_blank">专精特新中小企业2023届毕业生网上招聘活动</a><br />
            <a href="https://www.chsi.com.cn/jyzx/202308/20230802/2293109545.html" target="_blank">引导鼓励高校毕业生赴基层就业</a> {'\u00A0'}|{'\u00A0'} <a href="/jyzx/202308/20230816/2293106973.html" target="_blank">提供暖心服务 力促精准就业</a> {'\u00A0'}|{'\u00A0'}
            <a href="https://www.ncss.cn/ncss/zt/wxyjgl.shtml" target="_blank">毕业生五险一金全攻略</a>
            <div className="line01">{'\u00A0'}</div>
            <div className="font16">
              <a href="https://gaokao.chsi.com.cn/z/gkbmfslq/lqjg.jsp" target="_blank" className="colorBlue">高考录取日程及录取结果查询</a> {'\u00A0'}| {'\u00A0'}
              <a href="https://gaokao.chsi.com.cn/gkxx/zt/2023gkfzp.shtml" target="_blank" className="colorBlue">高考防诈骗指南</a></div>
            <a href="https://gaokao.chsi.com.cn/gkxx/zc/moe/202307/20230703/2293098248.html" target="_blank">教育部暑期高校学生资助热线电话开通</a> {'\u00A0'}| {'\u00A0'}
            <a href="https://gaokao.chsi.com.cn/gkxx/ksbd/202308/20230811/2293110107.html" target="_blank">准大学生你好：新的起点，你准备好了吗</a><br />
            <a href="/jyzx/202102/20210208/2028106580.html" target="_blank">套号学历涉嫌违法</a> {'\u00A0'}|{'\u00A0'}
            <a href="/zhaopin/index.jsp" target="_blank">学信网招聘</a> {'\u00A0'}|{'\u00A0'} <a href="/jyzx/xmt.shtml" target="_blank">新媒体矩阵</a> {'\u00A0'}|{'\u00A0'}
            <a href="/news/" target="_blank">【新闻】</a> {'\u00A0'}|{'\u00A0'}
            <a href="/zthz/" target="_blank">专题汇总</a></div> <div className="news-btm-list">
            <a href="/jyzx/202302/20230209/2256612160.html" className="news-item">关于做好2023年同等学力人员申请硕士学位外国语水平和...</a>
            <a href="/jyzx/202308/20230822/2293107345.html" className="news-item">广东工业大学：将实验室搬到乡村</a>
            <a href="/jyzx/202308/20230821/2293107278.html" className="news-item">把绿水青山变成“金山银山”——天津大学助力甘肃宕昌...</a>
          </div>
        </div>
        <div className="news-more">
          <a href="/jyzx/">更多
            <i className="van-icon van-icon-arrow"></i>
          </a>
        </div>
      </div>
      {/* 第三部分 */}
      <div className="index-part2">
        <a href="https://xz.chsi.com.cn/survey/wap/index.action">
          <img src="https://t4.chei.com.cn/chsi/assets/gb/wap/images/index/banner.png" />
        </a>
      </div>
      {/* 第四部分 */}
      <div className="index-part3"><div className="van-grid" style={{ paddingLeft: '10px' }}>
        <div className="van-grid-item" style={{ flexBasis: '33.3333%', paddingRight: '10px' }}>
          <div className="van-grid-item__content van-grid-item__content--center">
            <a onClick={() => { setShow(true) }}>
              <div className="bgUrl1" ></div>
              <div className="text"  > 学籍学历学位</div>
            </a>
          </div>
        </div>
        <div className="van-grid-item" style={{ flexBasis: '33.3333%', paddingRight: '10px' }}><div className="van-grid-item__content van-grid-item__content--center">
          <a href="/xlrz/index2.jsp">
            <div className="bgUrl2" >
            </div>
            <div className="text"> 出国教育背景信息服务</div>
          </a></div></div><div className="van-grid-item" >
          <div className="van-grid-item__content van-grid-item__content--center">
            <a href="https://my.chsi.com.cn/">
              <div className="bgUrl3" >
              </div>
              <div className="text"> 学信档案</div>
            </a>
          </div>
        </div>
        <div className="van-grid-item" >
          <div className="van-grid-item__content van-grid-item__content--center">
            <a href="https://gaokao.chsi.com.cn/">
              <div className="bgUrl4">
              </div>
              <div className="text"> 阳光高考</div>
            </a></div></div>
        <div className="van-grid-item" >
          <div className="van-grid-item__content van-grid-item__content--center">
            <a href="https://yz.chsi.com.cn/">
              <div className="bgUrl5" ></div>
              <div className="text"> 研招网</div>
            </a>
          </div>
        </div>
        <div className="van-grid-item">
          <div className="van-grid-item__content van-grid-item__content--center">
            <a href="https://www.gatzs.com.cn/">
              <div className="bgUrl6"></div>
              <div className="text"> 内地高校面向港澳台招生</div>
            </a>
          </div>
        </div>
        <div className="van-grid-item">
          <div className="van-grid-item__content van-grid-item__content--center">
            <a href="https://www.ncss.cn/"><div className="bgUrl7" ></div>
              <div className="text"> 国家大学生就业服务平台</div>
            </a>
          </div>
        </div>
        <div className="van-grid-item">
          <div className="van-grid-item__content van-grid-item__content--center">
            <a href="https://www.gfbzb.gov.cn/">
              <div className="bgUrl8" ></div>
              <div className="text"> 全国征兵网
              </div>
            </a>
          </div>
        </div>
        <div className="van-grid-item">
          <div className="van-grid-item__content van-grid-item__content--center">
            <a href="https://xz.chsi.com.cn/">
              <div className="bgUrl9"></div>
              <div className="text"> 学职平台</div>
            </a>
          </div>
        </div>
      </div>

      </div>
      {/* 第五部分 */}
      <div className='index-part4'>
        {renderPart4Block()}
      </div>
      {/* 官方微博 */}
      <div className='index-part5'>
        <div className="index-media-top">
          <h3 className="row-tit">官方微博 · 微信</h3>
          <a href="https://www.chsi.com.cn/jyzx/xmt.shtml">更多</a>
        </div>
        {renderMedia()}

      </div>

      {/* 弹窗 */}

      <div class="van-overlay" onClick={() => { setShow(false) }} style={{ display: show ? 'block' : 'none' }}>
        <div class="link-pop-wrapper">
          <div class="link-pop-cxt">
            <div className='guajbi'>
              <img src={xx} alt="" />
            </div>
            <div class="xjxl-link-pop">
              <div class="list-wrap">
                <div class="sub-tit">
                  <img src="https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-sub-xj.svg" />学籍</div>
                <div class="van-row"><div class="van-col van-col--8"><a href="/statusQuery">学籍查询</a>
                </div><div class="van-col van-col--8"><a href="/Query?name=xj">学籍验证</a>
                  </div></div>
              </div>
              <div class="list-wrap"><div class="sub-tit">
                <img src="https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-sub-xl.svg" />学历</div>
                <div class="van-row"><div class="van-col van-col--8"><a href="/Verify">学历查询</a></div>
                  <div class="van-col van-col--8"><a href="/Query?name=xl">学历验证</a></div><div class="van-col van-col--8">
                    <a href="/xlrz/index.jsp">学历认证</a>
                  </div>
                </div>
              </div>
              <div class="list-wrap">
                <div class="sub-tit">
                  <img src="https://t2.chei.com.cn/chsi/assets/gb/wap/images/index/icon-sub-xw.svg" />学位</div>
                <div class="van-row">
                  <div class="van-col van-col--8">
                    <a href="/xwcx/index.jsp">学位查询</a>
                  </div><div class="van-col van-col--8"><a href="/xlcx/bgcx.jsp">学位验证</a></div><div class="van-col van-col--8">
                    <a href="https://xwrz.chsi.com.cn/">学位认证</a></div></div></div></div></div></div></div>

      {/* 底部 */}
      <Footer />
      {/* 导航弹出层 */}
      {isShowPopup && (<Popup
        visible={isShowPopup}
        onMaskClick={() => {
          setIsShowPopup(false)
        }}
        onClose={() => {
          setIsShowPopup(false)
        }}
        position='top'
        bodyStyle={{ height: '' }}
      >
        <div className="my-close-bar">
          <a href="/wap/">
            <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="my-logo" />
          </a>
          <CloseOutline onClick={() => setIsShowPopup(false)} />
        </div>
        {navContent()}
      </Popup>)}
    </div>
  )
}

