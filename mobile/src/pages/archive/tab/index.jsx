
import './iconfont.css'
export const Tab = () => {
    return (
        <div className="">

            <div className="van-hairline--top-bottom van-tabbar van-tabbar--fixed" style={{ zIndex: '1000' }}>
                <div className="van-tabbar-item van-tabbar-item--active">
                    <div className="van-tabbar-item__icon">
                        <i title="home" className="iconfont"></i>
                    </div><div className="van-tabbar-item__text">
                        <span>首页</span></div></div> <div className="van-tabbar-item">
                    <div className="van-tabbar-item__icon">
                        <img src="https://t1.chei.com.cn/archive/images/xj/xjxlxw.svg" style={{ height: '0.613rem', margin: '-0.04rem 0px 0.04rem' }} />
                    </div><div className="van-tabbar-item__text">
                        <span>学籍学历学位</span></div></div>
                <div className="van-tabbar-item"><div className="van-tabbar-item__icon">
                    <i title="cp" className="iconfont"></i>
                </div><div className="van-tabbar-item__text">
                        <span>个人测评</span></div></div> <div className="van-tabbar-item">
                    <div className="van-tabbar-item__icon"><i title="zp" className="iconfont"></i>
                    </div><div className="van-tabbar-item__text"> <span>求职招聘</span>
                    </div>
                </div>
                <div className="van-tabbar-item">
                    <div className="van-tabbar-item__icon">
                        <i title="my" className="iconfont"></i>
                    </div><div className="van-tabbar-item__text"> <span>我的</span>
                    </div>
                </div>
            </div>

        </div>
    )
}