import { getData } from '../../../../utils/api'
import { useEffect, useState } from 'react';
import { Footer } from '../../../Footer'
import { CloseOutline, UnorderedListOutline } from 'antd-mobile-icons'

import { Swiper, Popup } from 'antd-mobile'

import { SWIPER, PART_FOUR_ITEMS, MEDIAS, NAV_ITEMS } from '../../../HomePage/consts'

import './index.css'

export const ReportXjBox = ({ activePage, setActivePage }) => {

    const [isShowPopup, setIsShowPopup] = useState(false)


    useEffect(() => {
        if (window.location.search.split('?id=')[1]) {
            getYanzeng()
        } else {
            getList()

        }

    }, []);

    const [dataSource, setDataSource] = useState({});
    const getList = async () => {
        const res = await getData('/api/user/zhengshu', 'post')
        if (res.data.code == 1) {
            setDataSource(res.data.data[0]);
        }
    }

    const getYanzeng = async () => {
        console.log(1);
        const res = await getData('/api/user/yanzheng', 'post', { online_code: window.location.search.split('?id=')[1] })
        if (res.data.code == 1) {
            setDataSource(res.data.data[0]);
        }
    }

    const navContent = () => {
        return (
            <div className='my-nav'>
                {NAV_ITEMS.map(item => {
                    return (<div className='nav-items'>
                        {item.map(e => {
                            return (
                                <div className="nav-items-item" key={e.key}>
                                    <a href={e.hrefUrl}>{e.text}</a>
                                </div>
                            )
                        })}
                    </div>)
                })}</div>
        )
    }


    return (
        <div className='bacscs'>
            <div className="wap-nav-bar"><div className="wap-nav-bar__left">
                <div >
                    <a href="/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="logo" />

                    </a>
                </div> <span className="logo-dot">

                </span> <span className="logo-text"><a href="/xlcx/bgcx.jsp">在线验证

                </a>

                </span>

            </div> <div className="wap-nav-bar__center">

                </div> <div className="wap-nav-bar__right"><span className="ch-nav">
                    <div className="wap-nav-bar__right" style={{ padding: '0' }} onClick={() => setIsShowPopup(true)}>
                        <span className="ch-nav"><UnorderedListOutline /></span>
                    </div>


                </span>

                </div>

            </div>
            <div className="res-wrap"><div className="card"><div className="top-change"><div className="lang-div">
                报告语种
                <a href="javascript:;" className="current">中文
                </a><a href="javascript:;">英文
                </a>
            </div> <div title="下载PDF" onclick="window.location.href='/report/xueli/download.do?vcode=ACZVDTBE6G03X3XY&amp;rid=53147208248462509361002610768517&amp;ln=cn'" className="pdf-div">
                </div>
            </div>  <h3 className="res-h3">教育部学籍证书电子注册备案表
                </h3> <div className="img-div"><img src={`http://154.83.14.37:81/${dataSource.avatar}`} width="58" data-caption="个人头像" />
                </div>
                <div class="rowcard">
                    <div class="row">
                        <div class="col-left">姓名</div><div class="col-right namtFamily">{dataSource.username}</div></div>
                    <div class="row"><div class="col-left">性别</div><div class="col-right">{dataSource.gender == 0 ? '女' : '男'}</div></div>
                    <div class="row"><div class="col-left">出生日期</div><div class="col-right">{dataSource.birthday}</div></div>
                    <div class="row"><div class="col-left">民族</div><div class="col-right">{dataSource.mingzu}</div></div>
                    <div class="row"><div class="col-left">证件号码</div><div class="col-right">{dataSource.id_number}</div></div>
                    <div class="row"><div class="col-left">院校</div><div class="col-right">{dataSource.school_name}</div></div>
                    <div class="row"><div class="col-left">层次</div><div class="col-right">{dataSource.xueli_level}</div></div>
                    <div class="row"><div class="col-left">院系</div><div class="col-right">{dataSource.fenyuan}</div></div>
                    <div class="row"><div class="col-left">班级</div><div class="col-right">{dataSource.class}</div></div>
                    <div class="row"><div class="col-left">专业</div><div class="col-right">{dataSource.zhuanye}</div></div>
                    <div class="row"><div class="col-left">学号</div><div class="col-right">{dataSource.xuehao}</div></div>
                    <div class="row"><div class="col-left">学制</div><div class="col-right">{dataSource.xuezhi}</div></div>
                    <div class="row"><div class="col-left">类型</div><div class="col-right">{dataSource.leibie}</div></div>
                    <div class="row"><div class="col-left">形式</div><div class="col-right">{dataSource.xingshi}</div></div>
                    <div class="row"><div class="col-left">入学日期</div><div class="col-right">{dataSource.enrollment_time}</div></div>
                    <div class="row"><div class="col-left">学籍状态</div><div class="col-right">{dataSource.is_biye = 0 ? '在校（离校日期：' : '毕业（毕业日期：'}{dataSource.graduation_time}）</div></div>
                    <div class="row"><div class="col-left">在线验证码</div><div class="col-right">{dataSource.online_code}</div></div>
                    <div class="row"><div class="col-left">更新日期</div><div class="col-right">{dataSource.updatetime}</div></div><div>

                    </div>
                </div>
            </div>
            </div>

            <Footer />
            {/* 导航弹出层 */}
            {isShowPopup && (<Popup
                visible={isShowPopup}
                onMaskClick={() => {
                    setIsShowPopup(false)
                }}
                onClose={() => {
                    setIsShowPopup(false)
                }}
                position='top'
                bodyStyle={{ height: '' }}
            >
                <div className="my-close-bar">
                    <a href="/wap/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="my-logo" />
                    </a>
                    <CloseOutline onClick={() => setIsShowPopup(false)} />
                </div>
                {navContent()}
            </Popup>)}
        </div >
    )
}