
import { ContentIndex } from "./index/index";
import { Gdjy } from "./Gdjy/index";
import { GdjyDetail } from "./GdjyDetail/index";
import { GdjyDetailTwo } from "./GdjyDetailTwo/index";
import { ReportBox } from "./ReportBox/index";
export const Content = ({ activePage, setActivePage }) => {
    console.log(activePage);
    switch (activePage) {
        case 'index':
            return <ContentIndex setActivePage={setActivePage} />;
        case 'gdjy':
            return <Gdjy setActivePage={setActivePage} />;
        case 'gdjydetail':
            return <GdjyDetail setActivePage={setActivePage} />;
        case 'GdjyDetailTwo':
            return <GdjyDetailTwo setActivePage={setActivePage} />;
        case 'ReportBox':
            return <ReportBox setActivePage={setActivePage} />;
        default:
            return null;
    }
};