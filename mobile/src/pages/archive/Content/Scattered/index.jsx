import { getData } from '../../../../utils/api'
import { useEffect, useState } from 'react';
import { Footers } from '../../../Footers'
import { CloseOutline, UnorderedListOutline } from 'antd-mobile-icons'

import { Swiper, Popup } from 'antd-mobile'

import { SWIPER, PART_FOUR_ITEMS, MEDIAS, NAV_ITEMS } from '../../../HomePage/consts'

import './index.css'

export const Scattered = ({ activePage, setActivePage }) => {

    const [isShowPopup, setIsShowPopup] = useState(false)
    const [username, setUsername] = useState('');
    const [onlinecode, setOnlinecode] = useState('');




    const goreport = () => {
        console.log(onlinecode.target.value);
        console.log(username.target.value);
        window.open('/gdjy/ReportBox?id=' + username.target.value+'&onlinecode='+onlinecode.target.value)

    }

    const navContent = () => {
        return (
            <div className='my-nav'>
                {NAV_ITEMS.map(item => {
                    return (<div className='nav-items'>
                        {item.map(e => {
                            return (
                                <div className="nav-items-item" key={e.key}>
                                    <a href={e.hrefUrl}>{e.text}</a>
                                </div>
                            )
                        })}
                    </div>)
                })}</div>
        )
    }


    return (
        <div className='bacscs' >
            <div className="wap-nav-bar"><div className="wap-nav-bar__left">
                <div >
                    <a href="/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="logo" />

                    </a>
                </div> <span className="logo-dot">

                </span> <span className="logo-text"><a href="/xlcx/bgcx.jsp">学历查询

                </a>

                </span>

            </div>
                <div className="wap-nav-bar__center">

                </div> <div className="wap-nav-bar__right"><span className="ch-nav">
                    <div className="wap-nav-bar__right" style={{ padding: '0' }} onClick={() => setIsShowPopup(true)}>
                        <span className="ch-nav"><UnorderedListOutline /></span>
                    </div>
                </span>
                </div>
            </div>
            <div style={{ padding: '0 0.427rem' }}>
                <h3 className="sub2-title">中国高等教育学历证书查询</h3>

                <form className="lscx-form van-form">
                    <div className="van-cell van-field van-field--label-right">
                        <div className="van-cell__title van-field__label van-field__label--right" style={{ width: '5.2em' }}>
                            <span>证书编号</span></div>
                        <div className="van-cell__value van-field__value"><div className="van-field__body">
                            <input type="text" name="zsbh" placeholder="学历证书或学历证明书编号" value={onlinecode.value}
                                onChange={(value) => setOnlinecode(value)} className="van-field__control" />

                        </div>
                        </div>
                    </div> <div className="van-cell van-field van-field--label-right">
                        <div className="van-cell__title van-field__label van-field__label--right" style={{ width: '5.2em' }}><span>姓名</span>
                        </div><div className="van-cell__value van-field__value"><div className="van-field__body">
                            <input type="text" name="xm" placeholder="学历证书或学历证明书上的姓名" value={username.value}
                                onChange={(value) => setUsername(value)} className="van-field__control" />
                        </div>
                        </div></div> <div className="van-cell van-field van-field--label-right">
                        <div className="van-cell__title van-field__label van-field__label--right" style={{ width: '5.2em' }}><span>图片验证码</span></div><div className="van-cell__value van-field__value"><div className="van-field__body">
                            <input type="text" name="yzm" placeholder="点击获取图片验证码" className="van-field__control" /></div></div>
                    </div>
                    <div className="lscx-button">
                        <button type="button" className="van-button van-button--info van-button--normal van-button--block buttt" onClick={goreport}>
                            <div className="van-button__content">
                                <span className="van-button__text">免费查询</span>
                            </div>
                        </button>
                        <div className="yhxy van-cell van-field van-field--label-right">
                            <div className="van-cell__value van-cell__value--alone van-field__value">
                                <div className="van-field__body"><div className="van-field__control van-field__control--custom">
                                    <div role="checkbox" tabindex="0" aria-checked="false" className="van-checkbox"><div className="van-checkbox__icon van-checkbox__icon--square"><i className="van-icon van-icon-success">
                                    </i></div><span className="van-checkbox__label">我已阅读并同意</span>
                                    </div>
                                    <a href="https://account.chsi.com.cn/account/help/agreement.jsp" target="_blank">《用户协议》</a>、<a href="https://account.chsi.com.cn/account/help/yszc.jsp" target="_blank">《隐私政策》</a>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div className="query-tips">
                    <h6 className="note">注意</h6> <p>1、点此查看 <a href="https://www.chsi.com.cn/help/cxfw.jsp" target="_blank" className="colorBlue">学历证书查询范围</a>。</p> <p>2、查询学历证书需经权属人同意。</p> <p>3、学历证书查询结果不得用于违背权属人意愿之用途。</p> <p>4、学历证书和证明书由各高校自行印制，证书编号通常为18位。不知道证书编号的，可咨询发证学校。</p></div>


            </div>

            <div style={{ position: 'fixed', bottom: '0', width: '100vw' }}>
                <Footers />
            </div>
            {/* 导航弹出层 */}
            {isShowPopup && (<Popup
                visible={isShowPopup}
                onMaskClick={() => {
                    setIsShowPopup(false)
                }}
                onClose={() => {
                    setIsShowPopup(false)
                }}
                position='top'
                bodyStyle={{ height: '' }}
            >
                <div className="my-close-bar">
                    <a href="/wap/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="my-logo" />
                    </a>
                    <CloseOutline onClick={() => setIsShowPopup(false)} />
                </div>
                {navContent()}
            </Popup>)}
        </div >
    )
}