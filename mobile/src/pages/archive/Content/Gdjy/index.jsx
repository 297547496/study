import { useEffect, useState } from 'react';
import { getData } from '../../../../utils/api'
import './index.css'

export const Gdjy = ({ activePage, setActivePage }) => {

  const [dataSource, setDataSource] = useState({});

  useEffect(() => {
    const getList = async () => {
      const res = await getData('/api/user/zhengshu', 'post')
      if (res.data.code == 1) {
        setDataSource(res.data.data[0]);
      }
    }
    getList()
  }, [])

  const bank = async () => {
    window.history.go(-1);
  }
  return (
    <div>
      <div className="green-header van-nav-bar van-hairline--bottom" style={{ zIndex: '1' }}>
        <div className="van-nav-bar__left"><i className="van-icon van-icon-arrow-left van-nav-bar__arrow" onClick={bank}>
        </i></div><div className="van-ellipsis van-nav-bar__title" style={{ color: '#fff' }}>高等教育信息</div><div className="van-nav-bar__right">
        </div></div>

      <div className="vote-part">
        <a href="https://my.chsi.com.cn/archive/survey/bygz/index.action?id=e2j74we29o61haqc&amp;from=gdjy_index_wap" id="vote_wap" className="vote-jy-part lxs-vote" style={{ display: 'block' }}>
          <div className="go-ahead two-line">
            <h4 id="vote_tit" className='text-l'>高校毕业生跟踪调查问卷</h4>
          </div>
        </a>
      </div>


      <div className="padding16 xjxl-list">
        <div className="list-tit van-row">
          <div className="l van-col van-col--8 text-l">学籍信息 (1)</div>
          <div className="r van-col van-col--16">还有学籍没有显示出来？
            <a href="javascript:;" style={{ color: '#26b887' }}>尝试绑定</a></div></div> <div className="xj list-card">
          <a href="/gdjy/detail">
            <p className='school'>
              <span className='school-name'>{dataSource.school_name}</span>
              <span className='xueli-level1'>{dataSource.xueli_level}</span>
            </p>
            <p className='zhuanyet'>
              <span>{dataSource.zhuanye} | </span>
              <span>{dataSource.xingshi}</span>
            </p>

          </a>
        </div>


        <div className="list-tit van-row" style={{display:dataSource.is_biye==0?'none':'block'}}>
          <div className="l van-col van-col--8 text-l">学历信息 (1)</div>
          <div className="r van-col van-col--16">还有学历没有显示出来？<a href="javascript:;" style={{ color: '#26b887' }}>尝试绑定</a>
          </div>
        </div>
        <div className="xl list-card" style={{display:dataSource.is_biye==0?'none':'block'}}>
          <a href="/gdjy/detail/two">
            <p className='school'>
              <span className='school-name'>{dataSource.school_name}</span>
              <span className='xueli-level2'>{dataSource.xueli_level}</span>
            </p>
            <p className='zhuanyet'>
              <span>{dataSource.zhuanye} | </span>
              <span>{dataSource.xingshi}</span>
            </p>
          </a>
        </div>



        <div className="list-tit van-row" style={{display:dataSource.is_biye==0?'block':'none'}}>
          <div className="l van-col van-col--8 text-l">学历信息 </div>
          <div className="r van-col van-col--16">还有学历没有显示出来？<a href="javascript:;" style={{ color: '#26b887' }}>尝试绑定</a>
          </div>
        </div>
        <div className="no-data" style={{display:dataSource.is_biye==0?'block':'none'}}>
          <h4 style={{ padding: ' 0px 0.3rem' }}>没有找到你的学历信息 <a href="http://www.chsi.com.cn/help/cxfw.jsp" target="_blank">
            <i title="查看" className="iconfont"></i>
          </a>
          </h4>
          <div className="find-xjxl-bottom">
            <span className="colorGreen">查看解决方法 <i title="展开" className="iconfont"></i>
            </span>
          </div>
        </div>





        <div className="list-tit van-row">
          <div className="l van-col van-col--8 text-l">学位信息 </div>
          <div className="r van-col van-col--16">还有学位没有显示出来？<a href="javascript:;" style={{ color: '#26b887' }}>尝试绑定</a>
          </div>
        </div>


        <div className="no-data">
          <h4 style={{ padding: ' 0px 0.3rem' }}>您还未绑定学位信息，可以使用“尝试绑定学位”功能绑定您的学位 <a href="http://www.chsi.com.cn/help/cxfw.jsp" target="_blank">
            <i title="查看" className="iconfont"></i></a></h4> <div className="find-xjxl"><div className="find-list van-row">
              <div className="van-col van-col--2"><span>1</span>
              </div>
              <div className="paddL2 van-col van-col--22">
                本系统提供社会查询的学位证书数据来源于相关学位授予单位经所在省（自治区、直辖市）学位委员会办公室报国务院学位委员会办公室备案的学位授予数据。
              </div></div> <div className="find-list van-row">
              <div className="van-col van-col--2">
                <span>2</span></div>
              <div className="paddL2 van-col van-col--22">
                请确保注册账号的“姓名”及“证件号码”无误，且对应的学位信息属于本系统<a href="http://www.chsi.com.cn/help/cxfw.jsp" target="_blank">查询范围</a>，若您未成功绑定学位信息，请与有关学位授予单位的学位管理部门联系核实。
              </div>
            </div>
            <div className="find-list van-row">
              <div className="van-col van-col--2">
                <span>3</span>
              </div>
              <div className="paddL2 van-col van-col--22">
                经核实后属学位授予信息漏报、错报的，由学位授予单位依照“学位授予信息补报、修改流程”更正相关内容。本网站无权修改任何数据。
              </div>
            </div>
          </div>
          <div className="find-xjxl-bottom">
            <span className="colorGreen">提示信息 <i title="展开" className="iconfont"></i></span></div></div> <div className="list-tit van-row"><div className="l van-col van-col--24">考研信息 </div></div> <div className="no-data list-card"><div className="yxmc">您没有考研信息！</div> <div className="des">您目前没有考研信息，系统提供2006年以来入学的硕士研究生报名和成绩数据。</div></div>       <div className="van-dialog" style={{ display: 'none' }}><div className="van-dialog__content"><div className="dialog-tip-div"><h3>没有学籍</h3>
              系统没有找到您的在校生学籍信息，有可能是因为您注册时填写的姓名和证件号码两项与学校上报的学籍注册数据不相同导致的。<br /><br />
              1、查看您注册时填写的姓名和证件号码信息 <a href="https://account.chsi.com.cn/account/account!show" target="_blank">点击查看</a><br />
              2、姓名和证件号码无误，我还有学籍没显示出来 <a href="javascript:;">找找看？</a></div></div><div className="van-hairline--top van-dialog__footer"><button className="van-button van-button--default van-button--large van-dialog__cancel" style={{ display: 'none' }}><span className="van-button__text">
                取消
              </span></button><button className="van-button van-button--default van-button--large van-dialog__confirm"><span className="van-button__text">
                确认
              </span>
            </button>
          </div>
        </div>
      </div>


    </div >
  )
};