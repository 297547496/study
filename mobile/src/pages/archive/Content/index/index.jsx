
import { useEffect, useState } from 'react';
import { getData } from '../../../../utils/api'
import './index.css'

export const ContentIndex = ({ activePage, setActivePage }) => {
    const [dataSource, setDataSource] = useState({});

    useEffect(() => {
        const getList = async () => {
            const res = await getData('/api/user/zhengshu', 'post')
            if (res.data.code == 1) {
                setDataSource(res.data.data[0]);
            }
        }
        getList()
    }, [])
    const goyanzheng = () => {
        if (window.localStorage.getItem('type') == 'xl') {
            window.location.href = '/gdjy/ReportBox'
        } else {
            window.location.href = '/gdjy/ReportXjBox'

        }

    }
    return (
        <div>
            <div className="my-header">
                <a href="/archive/wap/index.jsp" className="logo"></a>
                <a id="goOut" href="https://account.chsi.com.cn/passport/logout?service=https://my.chsi.com.cn/archive/index.jsp" className="go-out">
                    <i title="退出" className="iconfont"></i>
                </a>
            </div>

            <div className="vote-part">
                <a href="https://my.chsi.com.cn/archive/survey/bygz/index.action?id=e2j74we29o61haqc&amp;from=main-wap" id="vote_wap" className="vote-jy-part lxs-vote" style={{ display: 'block' }}>
                    <div className="go-ahead two-line">
                        <h4 id="vote_tit">高校毕业生跟踪调查问卷</h4>
                    </div>
                </a>
                <a href="https://xz.chsi.com.cn/cp/wap/cover.do?surveyId=9g0e9rpo48ejhbi3&amp;from=" target="_blank">
                    <div className="card-list van-row">
                        <div className="van-col van-col--4">
                            <span className="card-left-a">
                                <img src="https://t3.chei.com.cn/archive/images/xzpt/jhx.png" width="50" />
                            </span>
                        </div>
                        <div className="card-list-desc van-col van-col--14">
                            <h4 className="show-line2">计划能力测试</h4>
                        </div>
                        <div className="card-list-link van-col van-col--6">
                            参与测评<i className="van-icon van-icon-arrow">
                            </i>
                        </div>
                    </div>
                </a>
                <div className="card-list vote-myd van-row">
                    <div className="vote-myd-tit">
                        专业满意度
                        <span className="sub-tit">
                            {dataSource.school_name}-{dataSource.zhuanye}</span></div>
                    <div className="myd-bottom van-row"><div className="van-col van-col--16">
                        累计投票<span className="myd-totle">0</span>
                    </div>
                        <div className="van-col van-col--8">
                            <a href="https://my.chsi.com.cn/zyzsk/wap/zymyd/specappraisal">
                                <button className="van-button van-button--default van-button--normal van-button--round">
                                    <span className="van-button__text">我要评价</span>
                                </button>
                            </a></div>
                    </div>
                </div>
            </div>


            <div className="menu-part"><div className="van-row"><div className="van-col van-col--8">
                <a href="/active/gdjy" className="menu-a">
                    <span className="menu-img xjxl-img"></span><br />
                    高等教育信息
                </a></div> <div className="van-col van-col--8">
                    <a onClick={goyanzheng} className="menu-a">
                        <span className="menu-img zxyz-img"></span><br />
                        在线验证报告
                    </a></div> <div className="van-col van-col--8"><a href="https://my.chsi.com.cn/archive/rzbg/index.action" className="menu-a"><span className="menu-img rzbg-img"></span><br />
                        学历学位认证<br />与成绩验证
                    </a></div></div> <div className="van-row"><div className="van-col van-col--8"><a href="https://my.chsi.com.cn/archive/gjhz/index.action" className="menu-a"><span className="menu-img gjhz-img"></span><br />
                        出国报告发送
                    </a></div> <div className="van-col van-col--8"><a href="https://my.chsi.com.cn/archive/wap/gdjy/xj/collate.action" className="menu-a"><span className="menu-img txjd-img"></span><br />
                        毕业证书图像<br />校对
                    </a></div> <div className="van-col van-col--8"><a href="https://www.ncss.cn/" className="menu-a"><span className="menu-img xzy-img"></span><br />
                        就业
                    </a></div></div> <div className="van-row"><div className="van-col van-col--8"><a href="https://my.chsi.com.cn/archive/survey/index.action" className="menu-a"><span className="menu-img myd-img"></span><br />
                        学校满意度
                    </a></div> <div className="van-col van-col--8"><a href="http://xz.chsi.com.cn/survey/wap/index.action" className="menu-a"><span className="menu-img cp-img"></span><br />
                        个人测评
                    </a></div> <div className="van-col van-col--8"><a href="https://my.chsi.com.cn/archive/wap/auth/show.action" className="menu-a sq-num"> <span className="menu-img auth-img"></span><br />
                        信息核查确认
                    </a></div></div></div>


        </div>
    )
};