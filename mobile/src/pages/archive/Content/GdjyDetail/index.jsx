import { useEffect, useState } from 'react';
import { getData } from '../../../../utils/api'
import xlbj from '../../../../images/xlbj2.png'

import './index.css'

export const GdjyDetail = ({ activePage, setActivePage }) => {
    const [dataSource, setDataSource] = useState({});

    useEffect(() => {
        const getList = async () => {
            const res = await getData('/api/user/zhengshu', 'post')
            if (res.data.code == 1) {
                setDataSource(res.data.data[0]);
            }
        }
        getList()
    }, [])

    const bank = async () => {
        window.history.go(-1);
    }

    return (
        <div>
            <div className="white-header van-nav-bar van-hairline--bottom" style={{ zIndex: '1', background: '#fff' }}>
                <div className="van-nav-bar__left">
                    <i className="van-icon van-icon-arrow-left van-nav-bar__arrow" onClick={bank} style={{ color: '#333' }}>

                    </i>

                </div>
                <div className="van-ellipsis van-nav-bar__title" style={{ color: '#333' }}>高等学籍
                </div>
                <div className="van-nav-bar__right">

                </div>

            </div>

            <div className="padding16 xj-index">

                <div class="jd-res-div" style={{ marginBottom: '0.533rem', display: dataSource.is_biye == 0 ? 'block' : 'none' }}>
                    <div class="flex-box">
                        <div class="flex1" style={{ textAlign: 'left' }}>
                            <span class="scan-txewm" >
                            </span>
                            <span style={{ color: '#333' }}>
                                查看图像采集码
                            </span>
                        </div>
                        <div class="arrow-right">
                            <i class="iconfont "></i>
                        </div>
                    </div>
                    <div class="sanjiao"><em></em><span></span></div></div>

                <div className="top-card">


                    <div className="van-row">
                        <div className="van-col van-col--10" >
                            <div className="flex-box" >
                                <div className="" >
                                    <div className="img-div">
                                        <img src={`http://154.83.14.37:81/${dataSource.luqu_avatar}`} className="full-img" />
                                        <p className="text">录取照片
                                        </p>
                                    </div>
                                </div>
                                <div className="" style={{ marginLeft: '8px' }}>
                                    <div className="img-div">
                                        <img src={`http://154.83.14.37:81/${dataSource.avatar}`} style={{ display: dataSource.is_biye == 0 ? 'none' : 'inline-block' }} className="full-img" />
                                        <img src={xlbj} style={{ display: dataSource.is_biye == 0 ? 'inline-block' : 'none' }} className="full-img" />
                                        <span style={{ display: dataSource.is_biye == 0 ? 'inline-block' : 'none' }} class="red-tips-icon"></span>
                                        <p className="text">学历照片
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="van-col van-col--14"><h5 className="namtFamily">{dataSource.username}
                        </h5>
                            <p className='namtFamily'>
                                <span>{dataSource.gender == 0 ? '女' : '男'}</span>&emsp;
                                <span>{dataSource.birthday}</span>
                            </p>
                        </div>
                    </div>
                    <div className="top-bottom-img xj-detail-img">
                        <p className='school'>
                            <span className='school-name'>{dataSource.school_name}</span>
                            <span className='xueli-level1'>{dataSource.xueli_level}</span>
                        </p>
                        <p className='zhuanye'>
                            <span>{dataSource.zhuanye} &nbsp; | &nbsp; </span>
                            <span>{dataSource.xingshi}</span>
                        </p>

                    </div>
                </div>
                <div className="xj-c" >

                    <div style={{ marginTop: '30px' }}><div className='l-text'>民族</div> <div className='r-text'>{dataSource.mingzu}</div></div>
                    <div><div className='l-text'>证件号码</div> <div className='r-text'>{dataSource.id_number}</div></div>
                    <div><div className='l-text'>学制</div> <div className='r-text'>{dataSource.xuezhi}</div></div>
                    <div><div className='l-text'>学历类别</div> <div className='r-text'>{dataSource.category}</div></div>
                    <div><div className='l-text'>分院</div> <div className='r-text'>{dataSource.fenyuan}</div></div>
                    <div><div className='l-text'>系所</div> <div className='r-text'>{dataSource.xisuo}</div></div>
                    <div><div className='l-text'>班级</div> <div className='r-text'>{dataSource.class}</div></div>
                    <div><div className='l-text'>学号</div> <div className='r-text'>{dataSource.xuehao}</div></div>
                    <div><div className='l-text'>入学日期</div> <div className='r-text'>{dataSource.enrollment_time}</div></div>

                    <div style={{ display: dataSource.is_biye == 0 ? '' : 'none' }}>
                        <div className='l-text' >预计毕业日期</div> <div className='r-text'>{dataSource.graduation_time}</div>
                    </div>
                    <div style={{ display: dataSource.is_biye == 0 ? 'none' : '' }}>
                        <div className='l-text' >离校日期</div>    <div className='r-text'>{dataSource.graduation_time}</div>
                    </div>


                    <div><div className='l-text'>学籍状态</div> <div className='r-text'>{dataSource.is_biye == 0 ? '在籍（注册在籍）' : '不在籍（毕业）'}</div></div>

                </div>
                <div className="van-row" style={{ marginTop: '30px' }}>
                    <div className="van-col van-col--24">
                        <a>
                            <button className="my-btn van-button van-button--primary van-button--normal" onClick={() => { window.localStorage.setItem('type', 'xj'); alert('申请成功') }}>
                                <span className="van-button__text">申请验证报告
                                </span>
                            </button>
                        </a>
                    </div>
                </div>
            </div>




        </div >
    )
};