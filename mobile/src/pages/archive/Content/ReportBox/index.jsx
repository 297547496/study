import { getData } from '../../../../utils/api'
import { useEffect, useState } from 'react';
import { Footers } from '../../../Footers'
import { CloseOutline, UnorderedListOutline } from 'antd-mobile-icons'

import { Swiper, Popup } from 'antd-mobile'

import { SWIPER, PART_FOUR_ITEMS, MEDIAS, NAV_ITEMS } from '../../../HomePage/consts'

import './index.css'

export const ReportBox = ({ activePage, setActivePage }) => {

    const [isShowPopup, setIsShowPopup] = useState(false)


    useEffect(() => {
        if (window.location.search.split('?id=')[1]) {
            getYanzeng()
        } else {
            getList()

        }

    }, []);

    const [dataSource, setDataSource] = useState({});
    const getList = async () => {
        const res = await getData('/api/user/zhengshu', 'post')
        if (res.data.code == 1) {
            setDataSource(res.data.data[0]);
        }
    }

    const getYanzeng = async () => {
        console.log();
        const res = await getData('/api/user/yanzheng', 'post', { online_code: decodeURIComponent(window.location.search.split('?id=')[1].split('&onlinecode=')[1]) ,username:decodeURIComponent(window.location.search.split('?id=')[1].split('&onlinecode=')[0])})
        if (res.data.code == 1) {
            setDataSource(res.data.data[0]);
        }
    }

    const navContent = () => {
        return (
            <div className='my-nav'>
                {NAV_ITEMS.map(item => {
                    return (<div className='nav-items'>
                        {item.map(e => {
                            return (
                                <div className="nav-items-item" key={e.key}>
                                    <a href={e.hrefUrl}>{e.text}</a>
                                </div>
                            )
                        })}
                    </div>)
                })}</div>
        )
    }


    return (
        <div className='bacscs'>
            <div className="wap-nav-bar"><div className="wap-nav-bar__left">
                <div >
                    <a href="/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="logo" />

                    </a>
                </div> <span className="logo-dot">

                </span> <span className="logo-text"><a href="/xlcx/bgcx.jsp">在线验证

                </a>

                </span>

            </div> <div className="wap-nav-bar__center">

                </div> <div className="wap-nav-bar__right"><span className="ch-nav">
                    <div className="wap-nav-bar__right" style={{padding:'0'}} onClick={() => setIsShowPopup(true)}>
                        <span className="ch-nav"><UnorderedListOutline /></span>
                    </div>


                </span>

                </div>

            </div>
            <div className="res-wrap"><div className="card"><div className="top-change"><div className="lang-div">
                报告语种
                <a href="javascript:;" className="current">中文
                </a><a href="javascript:;">英文
                </a>
            </div> <div title="下载PDF" onclick="window.location.href='/report/xueli/download.do?vcode=ACZVDTBE6G03X3XY&amp;rid=53147208248462509361002610768517&amp;ln=cn'" className="pdf-div">
                </div>
            </div>  <h3 className="res-h3">教育部学历证书电子注册备案表
                </h3> <div className="img-div"><img src={`http://154.83.14.37:81/${dataSource.avatar}`} width="58" data-caption="个人头像" />
                </div> <div className="rowcard"><div className="row"><div className="col-left">姓名
                </div><div className="col-right namtFamily">{dataSource.username}
                    </div>
                </div> <div className="row"><div className="col-left">性别
                </div><div className="col-right">{dataSource.gender == 0 ? '女' : '男'}
                        </div>
                    </div> <div className="row"><div className="col-left">出生日期
                    </div><div className="col-right">{dataSource.birthday}
                        </div>
                    </div> <div className="row"><div className="col-left">入学日期
                    </div><div className="col-right">{dataSource.enrollment_time}
                        </div>
                    </div> <div className="row"><div className="col-left">毕(结)业日期
                    </div><div className="col-right">{dataSource.graduation_time}
                        </div>
                    </div> <div className="row"><div className="col-left">学校名称
                    </div><div className="col-right">{dataSource.school_name}
                        </div>
                    </div> <div className="row"><div className="col-left">专业
                    </div><div className="col-right">{dataSource.zhuanye}
                        </div>
                    </div> <div className="row"><div className="col-left">学制
                    </div><div className="col-right">{dataSource.xuezhi}
                        </div>
                    </div> <div className="row"><div className="col-left">层次
                    </div><div className="col-right">{dataSource.xueli_level}
                        </div>
                    </div> <div className="row"><div className="col-left">学历类别
                    </div><div className="col-right">{dataSource.leibie}
                        </div>
                    </div> <div className="row"><div className="col-left">学习形式
                    </div><div className="col-right">{dataSource.xingshi}
                        </div>
                    </div> <div className="row"><div className="col-left">毕(结)业
                    </div><div className="col-right">{dataSource.is_biye == 0 ? '结业' : '毕业'}
                        </div>
                    </div> <div className="row"><div className="col-left">证书编号
                    </div><div className="col-right">{dataSource.certificate_number}
                        </div>
                    </div> <div className="row"><div className="col-left">校(院)长姓名
                    </div><div className="col-right">{dataSource.rector}
                        </div>
                    </div> <div className="row"><div className="col-left">在线验证码
                    </div><div className="col-right">{dataSource.online_code}
                        </div>
                    </div> <div className="row"><div className="col-left">更新日期
                    </div><div className="col-right">{dataSource.updatetime}
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <Footers />
            {/* 导航弹出层 */}
            {isShowPopup && (<Popup
                visible={isShowPopup}
                onMaskClick={() => {
                    setIsShowPopup(false)
                }}
                onClose={() => {
                    setIsShowPopup(false)
                }}
                position='top'
                bodyStyle={{ height: '' }}
            >
                <div className="my-close-bar">
                    <a href="/wap/">
                        <img src="https://t3.chei.com.cn/chsi/assets/gb/wap/images/logo.png" alt="学信网" className="my-logo" />
                    </a>
                    <CloseOutline onClick={() => setIsShowPopup(false)} />
                </div>
                {navContent()}
            </Popup>)}
        </div >
    )
}