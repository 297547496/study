import { useEffect, useState } from 'react';
import { getData } from '../../../../utils/api'
import '../GdjyDetail/index'

export const GdjyDetailTwo = ({ activePage, setActivePage }) => {

    const [dataSource, setDataSource] = useState({});

    useEffect(() => {
        const getList = async () => {
            const res = await getData('/api/user/zhengshu', 'post')
            if (res.data.code == 1) {
                setDataSource(res.data.data[0]);
            }
        }
        getList()
    }, [])


    const bank = async () => {
        window.history.go(-1);
    }
    return (
        <div>
            <div className="white-header van-nav-bar van-hairline--bottom" style={{ zIndex: '1', background: '#fff' }}>
                <div className="van-nav-bar__left" >
                    <i className="van-icon van-icon-arrow-left van-nav-bar__arrow" style={{ color: '#333' }} onClick={bank}>


                    </i>


                </div>
                <div className="van-ellipsis van-nav-bar__title" style={{ color: '#333' }}>高等学历

                </div>
                <div className="van-nav-bar__right">


                </div>


            </div>

            <div className="padding16 xj-index"><div className="top-card xl-top-card"><div className="van-row"><div className="van-col van-col--5"><div className="img-div">
                <img src={`http://154.83.14.37:81/${dataSource.avatar}`} />
            </div>
            </div> <div className="van-col van-col--19"><h5 className="namtFamily">{dataSource.username}
            </h5> <p className="namtFamily">    <span>{dataSource.gender == 0 ? '女' : '男'}</span>&emsp;
                        <span>{dataSource.birthday}</span>
                    </p>
                </div>
            </div>
                <br />
                <div className="top-bottom-img xj-detail-img">
                    <p className='school'>
                        <span className='school-name'>{dataSource.school_name}</span>
                        <span className='xueli-level1' style={{ background: '#2b77c7' }}>{dataSource.xueli_level}</span>
                    </p>
                    <p className='zhuanye'>
                        <span>{dataSource.zhuanye} &nbsp; | &nbsp; </span>
                        <span>{dataSource.xingshi}</span>
                    </p>

                </div>
            </div>

                <div className="xj-c">
                    <div><div className='l-text'>入学日期</div> <div>{dataSource.enrollment_time}</div></div>
                    <div><div className='l-text'>毕（结）业日期</div> <div>{dataSource.graduation_time}</div></div>
                    <div><div className='l-text'>学历类别</div> <div>{dataSource.leibie}</div></div>
                    <div><div className='l-text'>学制</div> <div>{dataSource.xuezhi}</div></div>
                    <div><div className='l-text'>毕（结）业</div> <div>{dataSource.is_biye == 0 ? '结业' : '毕业'}</div></div>
                    <div><div className='l-text'>校（院）长姓名</div> <div>{dataSource.rector}</div></div>
                    <div><div className='l-text'>证书编号</div> <div>{dataSource.certificate_number}</div></div>

                </div>


                <div className="van-row"><div className="van-col van-col--24"><a>

                    <button className="my-btn van-button van-button--primary van-button--normal" onClick={() => { window.localStorage.setItem('type', 'xl'); alert('申请成功') }}><span className="van-button__text">申请验证报告
                    </span>
                    </button>
                </a>
                </div>
                </div>
            </div>





        </div >
    )
};