
import { Tab } from './tab'
import { Content } from './Content'

export const ArchiveIndex = () => {
    return (
        <div className="">
            <Content activePage={'index'}></Content>
            <Tab></Tab>
        </div>
    )
}