import { HomePage } from "../pages/HomePage";
import { LoginPage } from "../pages/Login";
import { ArchivePage } from "../pages/myChsi/archive";
import { ArchiveLoginPage } from "../pages/myChsi/login";
import { ArchiveIndexPage } from "../pages/myChsi/index";
import { Report } from "../pages/HomePage/Content/Report";
import { XlReport } from "../pages/HomePage/Content/XlReport";
import { IndexBox } from "../pages/myChsi/Content/indexBox";
import { XjBox } from "../pages/myChsi/Content/XjBox";
import { XlBox } from "../pages/myChsi/Content/XlBox";
import { XwBox } from "../pages/myChsi/Content/XwBox";
import { ZxyzBox } from "../pages/myChsi/Content/ZxyzBox";
import { ZxyzListBox } from "../pages/myChsi/Content/ZxyzListBox";

const route = [
	{ path: "/", name: "首页", components: HomePage },
	{ path: "/index", name: "首页", components: HomePage },
	{ path: "/login", name: "登录页", components: LoginPage },
	{ path: "/my.chsi/archive", name: "学信档案", components: ArchivePage },
	{ path: "/my.chsi/login", name: "档案登录", components: ArchiveLoginPage },
	{ path: "/my.chsi/index", name: "学信档案", components: ArchiveIndexPage },
	{ path: "/Reports", name: "学信档案", components: Report },
	{ path: "/XlReport", name: "学信档案", components: XlReport },
	{ path: "/my.chsi/px", name: "学籍", components: IndexBox }, // Todo 改路由名字
	{ path: "/my.chsi/xj", name: "学籍", components: XjBox },
	{ path: "/my.chsi/xl", name: "学历", components: XlBox },
	{ path: "/my.chsi/xw", name: "学位", components: XwBox },
	{ path: "/my.chsi/zxyz/list", name: "在线验证报告", components: ZxyzBox },
	{ path: "/my.chsi/zxyz/apply", name: "在线验证报告申请", components: ZxyzListBox },
];

export const routers = [...route];
