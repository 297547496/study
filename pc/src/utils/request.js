import axios from 'axios'
axios.defaults.headers.common["Content-Type"] = 'application/x-www-form-urlencoded';
var req = axios.create({
    baseURL: "http://admin.cxl1.cn",
    timeout: 5000,
    headers: { // 设置请求头
        'Content-Type': 'application/x-www-form-urlencoded',
        'Token': window.localStorage.getItem('token'), // 设置认证头部
    }
})

// 添加请求拦截器
req.interceptors.request.use((config) => {
    return config;
}, function (error) {
    return Promise.reject(error);
});



// 添加响应拦截器
req.interceptors.response.use((response) => {
    return response;
}, function (error) {
    return Promise.reject(error);
});

export default req
