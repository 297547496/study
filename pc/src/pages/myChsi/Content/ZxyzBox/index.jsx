import './index.css'
export const ZxyzBox = ({ setActivePage }) => {
  return (
    <div className='mt30'>
      <div className="main-block zxyz-container border-shadow">
        <ul className="zxyzbg-home-list" style={{ width: '1132px' }}>
          <li className="clearfix" >
            <div className="list-l">
              <h5>教育部学籍在线验证报告</h5>
              <h6>Online Verification Report of Student Record (Current Students) </h6>
              <p className="desc">《教育部学籍在线验证报告》是依据《高等学校学生学籍学历电子注册办法》（<a href="http://www.chsi.com.cn/jyzx/201408/20140829/1245955796.html" target="_blank">教学[2014]11号</a>）为学生本人提供的学籍注册信息网上查询验证服务。</p>
            </div>
            <div className="list-r">
              <a className="green-btn mid-btn" onClick={() => {
                setActivePage('zxyzlist')
              }}>查看</a>
            </div>
          </li>

          <li className="clearfix">
            <div className="list-l">
              <h5>教育部学历证书电子注册备案表</h5>
              <h6>Online Verification Report of HE Qualification Certificate (Graduate Students)</h6>
              <p className="desc">《教育部学历证书电子注册备案表》是依据《高等学校学生学籍学历电子注册办法》（<a href="http://www.chsi.com.cn/jyzx/201408/20140829/1245955796.html" target="_blank">教学[2014]11号</a>）对学生本人提供的学历注册信息网上查询验证服务。</p>
            </div>
            <div className="list-r">
              <a className="green-btn mid-btn" onClick={() => {
                setActivePage('zxyzlist')
              }}>查看</a>
            </div>
          </li>

          <li className="clearfix">
            <div className="list-l">
              <h5>中国高等教育学位在线验证报告</h5>
              <h6>Online Verification Report of Higher Education Degree Certificate</h6>
              <p className="desc">《中国高等教育学位在线验证报告》是依据《学位授予信息管理工作规程》（学位办〔2020〕8号）对学生本人提供的学位授予信息网上查询验证服务。</p>
            </div>
            <div className="list-r">
              <a className="green-btn mid-btn" onClick={() => {
                setActivePage('zxyzlist')
              }}>查看</a>
            </div>
          </li>
        </ul>

        <div className="zxyz-i-b">
          <h6><i className="iconfont bgjs"></i>报告介绍</h6>
          <p>为满足求职招聘、派遣接收、升学（考研、专升本）、出国留学、干部任免、职称评定、信用评估等领域的需要，学信网依托全国高等教育学生信息数据库，对学生的学籍、学历、学位、招生录取等相关信息提供在线验证报告，如：《教育部学历证书电子注册备案表》、《教育部学籍在线验证报告》、《中国高等教育学位在线验证报告》等。验证报告由学信网提供在线验证功能，报告持有人登录网站在线验证页面，输入在线验证码即可免费验证报告内容。报告中的信息也可通过扫描二维验证码进行验证或手机上网再验证。报告可在验证有效期内多次打印、多次验证。</p>
          <a href="http://www.chsi.com.cn/xlcx/bgys.jsp" target="_blank">报告简介</a>
          <a href="http://www.chsi.com.cn/xlcx/tdhyt.jsp" target="_blank">特点和用途</a>
          <a href="http://www.chsi.com.cn/xlcx/rhsq.jsp" target="_blank">如何申请</a>
          <a href="http://www.chsi.com.cn/xlcx/tbsm.jsp" target="_blank">特别声明</a>
        </div>
      </div>
    </div>
  )
};