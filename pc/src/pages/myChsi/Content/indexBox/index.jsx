
import { Link } from 'react-router-dom'

import "./index.css";
import "./archive-1.0.2.min.css";

export const IndexBox = ({ setActivePage }) => {
  return (
    <div>

      <div id="surveyPicInfo" className="vote_part vote_bg_lxhjy" style={{ display: 'block' }}>
        <a href="#ptgxbysjyzk_survey">高校毕业生跟踪调查问卷</a>
      </div>

      <div className="main-index clearfix">
        <div className="m-left index-left">
          <div className="index-main border-shadow ">
            <ul className="clearfix main-entry">
              <li className="i-m-top clearfix">
                <div className="i-m-l"><span className="c-img"></span></div>
                <div className="i-m-r">
                  <span>高等教育信息</span><br />
                  <a onClick={() => {
                    setActivePage('xj')
                  }}>学籍</a>
                  &emsp;
                  <a onClick={() => {
                    setActivePage('xl')
                  }}>学历</a>&emsp;
                  <a onClick={() => {
                    setActivePage('xw')
                  }}>学位</a>
                </div>
              </li>
              <li className="i-m-top clearfix">
                <div className="i-m-l"><span className="c-img zxyz"></span></div>
                <div className="i-m-r">
                  <span>在线验证报告</span><br />
                  <a onClick={() => {
                    setActivePage('zxyz')
                  }}>查看</a>&emsp;
                  <a  onClick={() => {
                    setActivePage('zxyz')
                  }}>申请</a>
                </div>
              </li>
              <li className="i-m-top clearfix">
                <div className="i-m-l"><span className="c-img xlrz"></span></div>
                <div className="i-m-r">
                  <span>学历学位认证与成绩验证</span><br />
                  <a href="rzbg/index.action">查看</a>
                </div>
              </li>
              <li className="i-m-top clearfix">
                <div className="i-m-l i-m-l4"><span className="c-img gjhz"></span></div>
                <div className="i-m-r">
                  出国报告发送<br />
                  <a href="gjhz/index.action">查看</a>　<a href="gjhz/apply.action">发送报告</a>　<a href="gjhz/foreign/index.action">翻译件</a>
                </div>
              </li>
              <li className="i-m-top i-m-no-right clearfix">
                <div className="i-m-l i-m-l6"><span className="c-img txjd"></span></div>
                <div className="i-m-r">
                  毕业证书图像校对<br />
                  <a href="gdjy/xj/show.action">校对</a>
                </div>
              </li>
              <li className="i-m-bom clearfix">
                <div className="i-m-l i-m-l4"><span className="c-img ncss"></span></div>
                <div className="i-m-r">
                  就业<br />
                  <a href="https://www.ncss.cn/" onmousedown="gaTrackerOutboundLink(this,'ncssindex', 'click', 'fromIndexMenu');zhugeFun.goNcss('fromIndexMenu','ncssindex')" target="_blank">求职招聘</a>
                </div>
              </li>
              <li className="i-m-bom clearfix">
                <div className="i-m-l i-m-l5"><span className="c-img vote"></span></div>
                <div className="i-m-r">
                  学校满意度<br />
                  <a href="survey/index.action">参与投票</a>
                </div>
              </li>
              <li className="i-m-bom clearfix">
                <div className="i-m-l i-m-l4"><span className="c-img xzpt"></span></div>
                <div className="i-m-r">
                  个人测评<br />
                  <a href="http://xz.chsi.com.cn/survey/index.action" target="_blank">进入</a>
                </div>
              </li>
              <li className="i-m-bom clearfix">
                <div className="i-m-l i-m-l4 main-sqgl"><span className="c-img sqgl"></span><span id="sqNum" className="sq-num" style={{ display: 'none' }}></span></div>
                <div className="i-m-r">
                  信息核查确认<br />
                  <a href="https://my.chsi.com.cn/archive/auth/show.action">查看</a>
                </div>
              </li>
            </ul>
          </div>

          <div id="jycyContent" className="iview-survey survey-cxt">
            <div className="survey-main">
              <h5 className="title">高校毕业生跟踪调查问卷</h5>
              <div className="des" style={{ display: 'block' }} >为了解高校毕业生职业发展现状，帮助大学生做好就业指导，请您参与本次调查，真诚感谢您的参与！</div>
              <div id="itemCxt0" className="item-cxt">
                <p>1. 您现在处于何种状态</p>
                <div name="ivuRadioGroup_1693235502820_0" className="ivu-radio-group">
                  <label className="ivu-radio-wrapper ivu-radio-group-item">
                    <span className="ivu-radio"><span className="ivu-radio-inner"></span>
                      <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                    </span>已工作</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"><span className="ivu-radio-inner"></span>
                      <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                    </span>已创业</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"><span className="ivu-radio-inner"></span>
                      <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                    </span>确定或已升学</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"><span className="ivu-radio-inner"></span>
                      <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                    </span>确定或已出国</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"><span className="ivu-radio-inner"></span>
                      <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                    </span>求职中</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"
                    ><span className="ivu-radio-inner"></span> <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                    </span>暂不就业</label></div></div>
            </div>
            <div className="btn-part"><input type="button" value="提交" id="submitButton" className="green-btn big-btn" /></div>
          </div>
        </div>
        <div className="m-right index-right border-shadow marginB0" id="xzpt_survey" style={{ height: '546px' }}>

          <img src="https://t3.chei.com.cn/archive/images/xzpt/jhx.png" alt="" width="190" height="190" />
          <p className="test-title" id="testTitle" style={{ fontSize: '14px' }}>
            混乱或秩序，你更偏爱哪一方
          </p>
          <a className="test-btn" href="http://xz.chsi.com.cn/cp/sph/cover.do?sphSurveyId=9g0e9rpo48ejhbi3" target="_blank">
            点击进入计划性测评
          </a>

          <img src="https://t3.chei.com.cn/archive/images/dctp/cp_ewm.png?20190306" alt="职业调查" title="职业调查" className="index_zycp_img" />
          <p className="index_zycp_text">学信网测评</p>
        </div>

      </div>

    </div>
  )
}
