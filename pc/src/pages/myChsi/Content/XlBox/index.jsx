import React, { useEffect, useState } from 'react';
import { getData } from '../../../../utils/api';
import show from "../../../../static/img/show.png";
import dp from "../../../../static/img/dp.png";
import './index.css'
export const XlBox = () => {

  useEffect(() => {
    getList()
  }, []);

  const [list, setList] = useState({})

  const getList = async () => {
    const res = await getData('/api/user/zhengshu', 'post')
    if (res.data.code == 1) {
      setList(res.data.data[0]);
    }
  }



  return (
    <div>
      <div id="surveyPicInfo" className="vote_part vote_bg_lxhjy" style={{ display: 'block' }}>
        <a href="#ptgxbysjyzk_survey">高校毕业生跟踪调查问卷</a>
      </div>
      <div className="xj_top">
        <span className="find_xj">您一共有<span className="xj_num"> 1 </span>个学历</span>
        还有学历没有显示出来？ <a href="###" onclick="bindxjreport();return false;">尝试绑定学历</a>　|　<a href="http://www.chsi.com.cn/help/cxfw.jsp" target="_blank">学历查询范围</a>
      </div>
      <div className="dpf aic jcsb">
        <div className="m-left xj-left main-block border-shadow marginB0" style={{ height: '500px', padding: ' 10px 0' }}>
          <div className="dpf aic jcsb " >
            <div className="mb-title ">
              <div className="xxmc">
              {list.xueli_level}-{list.school_name}-{list.zhuanye}
              </div>
            </div>
            <span className="mb-title-a mr20 dpf aic">
              <img src={dp} />
              &nbsp;
              <a href="/archive/bab/xj/show.action?trnd=54740031624914117658083434484579#zxyz_1" onmousedown="zhugeFun.gdjyToZxyz('学历','pc');">查看该学历的在线验证报告</a></span>
          </div>
          <div className="xj-l-m clearfix">
            <div className="xj-m-left">
              <div className="pic">
                <img src={'http://154.83.14.37:81/' + list.luqu_avatar} align="absmiddle" width="120" height="160" alt="录取照片" />
              </div>

            </div>
            <div className="xj-m-r">
              <div className="dpf fw xl-box fs12 c000 xlbj">
                <div className="dpf w50x txt_c aic"> <div className="w50x txt_r c99e">姓名：</div><div>{list.nickname}</div></div>
                <div className="w50x txt_c dpf aic"><div className="w50x txt_r c99e">性别：</div> <div>{list.gender==0?'女':'男'}</div></div>
                <div className="dpf w50x txt_c aic"> <div className="w50x txt_r c99e">出生日期：</div><div>{list.birthday}</div></div>
                <div className="w50x txt_c dpf aic"><div className="w50x txt_r c99e">入学时间：</div> <div>{list.enrollment_time}</div></div>
                <div className="dpf w50x txt_c aic"> <div className="w50x txt_r c99e">毕（结）业日期：</div><div>{list.graduation_time}</div></div>
                <div className="w50x txt_c dpf aic"><div className="w50x txt_r c99e">学校名称：</div> <div>{list.school_name}</div></div>
                <div className="w50x txt_c dpf aic"><div className="w50x txt_r c99e">专业：</div> <div>{list.zhuanye}</div></div>
                <div className="dpf w50x txt_c aic"> <div className="w50x txt_r c99e">学历类别：</div><div>{list.leibie}</div></div>
                <div className="dpf w50x txt_c aic"> <div className="w50x txt_r c99e">学制：</div><div>{list.xuezhi}</div></div>
                <div className="w50x txt_c dpf aic"><div className="w50x txt_r c99e">学历形式：</div> <div>{list.xingshi}</div></div>
                <div className="dpf w50x txt_c aic"> <div className="w50x txt_r c99e">层次：</div><div>{list.xueli_level}</div></div>
                <div className="w50x txt_c dpf aic"><div className="w50x txt_r c99e">毕（结）业：</div> <div>{list.is_biye==1?'毕业':'未毕业)'}</div></div>
                <div className="dpf w50x txt_c aic"> <div className="w50x txt_r c99e">校（院）长姓名：</div><div>{list.rector}</div></div>
                <div className="w50x txt_c dpf aic"><div className="w50x txt_r c99e">证书编号：</div> <div>{list.certificate_number}</div></div>
              </div>
            </div>
            <div className="clearFloat">&nbsp;</div>
          </div>

        </div>
        <div className="m-right border-shadow marginB0" style={{ height: '500px', padding: ' 10px 0' }}>
          <div id="myd_dv98pxjnb58mq84t">
            <div className="clearfix dl">
              <div className="dt">专业推荐</div>

              <div className="dd">
                <span><span className="ps">累计投票</span>2472</span>
              </div>
              <div className="clearFloat">&nbsp;</div>
              <div className="major">

                您尚未推荐专业


              </div>

              <div className="tj"><a href="https://my.chsi.com.cn/zyzsk/zytj/recommendContinue.action?trnd=59374783383657960846310826198337" onmousedown="zhugeFun.xjxlMydClick('专业推荐')" target="_blank">我要推荐</a></div>
            </div>

            <div className="clearfix dl">
              <div className="dt">专业满意度</div>

              <div className="tj"><a href="https://my.chsi.com.cn/zyzsk/myd/specAppraisalWelcome.action?trnd=59374783383657960846310826198337" onmousedown="zhugeFun.xjxlMydClick('专业满意度')" target="_blank">我要评价</a></div>
            </div>

            <div className="clearfix dl noborder">
              <div className="dt">院校满意度</div>

              <div className="tj"><a href="https://my.chsi.com.cn/zyzsk/myd/schAppraisalWelcome.action?trnd=59374783383657960846310826198337" onmousedown="zhugeFun.xjxlMydClick('院校满意度')" target="_blank">我要评价</a></div>
            </div>
          </div>
          <div className="txt_c">
            <a href="http://chsi.wanfangtech.net/" target="_blank" onmousedown="clickLwcc(this)" className="lwcc-rk-btn"><img src="https://t1.chei.com.cn/archive/images/xj/lwcx-1.png" alt="毕业论文查重" width="230" /></a>
            <a href="https://my.chsi.com.cn/archive/zybh/show.action" target="_blank" className="green-btn big-btn zybh-rk-btn mt10" style={{ width: '170px', borderRadius: '6px' }}>学科/专业变化查询</a>
          </div>
        </div>
      </div>
      <div className="width1180x mt20" style={{ marginTop: '20px' }}>
        <div id="jycyContent" className="iview-survey survey-cxt">
          <div className="survey-main">
            <h5 className="title">高校毕业生跟踪调查问卷</h5>
            <div className="des" style={{ display: 'block' }} >为了解高校毕业生职业发展现状，帮助大学生做好就业指导，请您参与本次调查，真诚感谢您的参与！</div>
            <div id="itemCxt0" className="item-cxt">
              <p>1. 您现在处于何种状态</p>
              <div name="ivuRadioGroup_1693235502820_0" className="ivu-radio-group">
                <label className="ivu-radio-wrapper ivu-radio-group-item">
                  <span className="ivu-radio"><span className="ivu-radio-inner"></span>
                    <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                  </span>已工作</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"><span className="ivu-radio-inner"></span>
                    <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                  </span>已创业</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"><span className="ivu-radio-inner"></span>
                    <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                  </span>确定或已升学</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"><span className="ivu-radio-inner"></span>
                    <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                  </span>确定或已出国</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"><span className="ivu-radio-inner"></span>
                    <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                  </span>求职中</label><label className="ivu-radio-wrapper ivu-radio-group-item"><span className="ivu-radio"
                  ><span className="ivu-radio-inner"></span> <input type="radio" className="ivu-radio-input" name="ivuRadioGroup_1693235502820_0" />
                  </span>暂不就业</label></div></div>
          </div>
          <div className="btn-part"><input type="button" value="提交" id="submitButton" className="green-btn big-btn" /></div>
        </div>
      </div>
    </div>
  )
};