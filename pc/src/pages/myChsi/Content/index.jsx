import { IndexBox } from './indexBox';
import { XjBox } from './XjBox'
import { XlBox } from './XlBox'
import { XwBox } from './XwBox'
import { KyBox } from './KyBox'
import { ZxyzBox } from './ZxyzBox'
import { ZxyzListBox } from './ZxyzListBox'


export const Content = ({ activePage,setActivePage }) => {
  
  console.log(activePage);
  switch (activePage) {
    case 'index':
      return <IndexBox setActivePage={setActivePage} />;
    case 'xj':
      return <XjBox setActivePage={setActivePage}/>;
    case 'xl':
      return <XlBox setActivePage={setActivePage}/>;
    case 'xw':
      return <XwBox setActivePage={setActivePage}/>;
    case 'ky':
      return <KyBox setActivePage={setActivePage}/>;
    case 'zxyz':
      return <ZxyzBox setActivePage={setActivePage}/>;
    case 'zxyzlist':
      return <ZxyzListBox setActivePage={setActivePage}/>;
    default:
      return null;
  }
};