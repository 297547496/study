import './index.css'
export const KyBox = () => {
  return (
    <div className='mt30'>

      <div className="main-block border-shadow pb20">
        <div className="no-info">
          <div className="no-info-content" style={{ paddingRight: '100px' }} >
            <h5>您没有考研信息！</h5>
            <p>您目前没有考研信息，系统提供2006年以来入学的硕士研究生报名和成绩数据。</p>
          </div>
        </div>
      </div>
    </div>
  )
};