import './index.css'
export const XwBox = () => {
  return (
    <div className='mt30'>

      <div className="main-block border-shadow pb20">
        <div className="no-info">
          <div className="no-info-content">
            <h5 className="font18">您还未绑定学位信息，可以使用“尝试绑定学位”功能绑定您的学位</h5>
            <p><a href="javascript:;" onclick="bindxwreport();return false;">尝试绑定学位</a>　|　<a href="http://www.chsi.com.cn/help/cxfw.jsp" className="cxfw" target="_blank">学位查询范围</a></p>
          </div>
          <div className="mb-tips">
            <i className="iconfont" title="提示信息"></i>
            <h6>提示信息</h6>
            <div className="mb-tips-content">
              1、本系统提供社会查询的学位证书数据来源于相关学位授予单位经所在省（自治区、直辖市）学位委员会办公室报国务院学位委员会办公室备案的学位授予数据。<br />
              2、请确保注册账号的“姓名”及“证件号码”无误，且对应的学位信息属于本系统<a href="http://www.chsi.com.cn/help/cxfw.jsp" className="cxfw" target="_blank">查询范围</a>，若您未成功绑定学位信息，请与有关学位授予单位的学位管理部门联系核实。<br />
              3、经核实后属学位授予信息漏报、错报的，由学位授予单位依照“学位授予信息补报、修改流程”更正相关内容。本网站无权修改任何数据。
            </div>
          </div>

        </div>
      </div>
    </div>
  )
};