import React, { useEffect, useState } from 'react';
import { getData } from '../../../../utils/api';
import './index.css'
export const ZxyzListBox = () => {

  useEffect(() => {
    getList()
  }, []);

  const [list, setList] = useState({})

  const getList = async () => {
    const res = await getData('/api/user/zhengshu', 'post')
    if (res.data.code == 1) {
      setList(res.data.data[0]);
    }
  }




  return (
    <div className='mt30'>
      <div className="main-block border-shadow" name="zxyz_1" id="zxyz_1">
        <br />
        <div className="clearfix">
          <div className="xxmc">
            {list.xueli_level + '-' + list.school_name}
          </div>
        </div>
        <div className="bg_mian">
          <div className="bg_tit"><i className="iconfont"></i>已申请的报告</div>
          <table className="zxyz_table" id="zxyz_table_1">
            <tbody><tr>
              <th width="206">在线验证码</th>
              <th width="130">语种</th>
              <th width="200">有效期</th>
              <th width="280">状态</th>
              <th>操作</th>
            </tr>
              <tr className="hasborder">
                <td>
                  <a className="blue" href={'/XlReport?id=' + list.online_code} target="_blank">{list.online_code}</a>
                </td>
                <td>中文</td>
                <td>2023-09-25</td>
                <td>有效</td>
                <td>
                  <a className="blue" href={'/XlReport?id=' + list.online_code} target="_blank">查看</a>

                  <a href="https://my.chsi.com.cn/archive/bab/xj/prolong.action?bgid=dsr4f5mq5ysfn54o">延长验证有效期</a>


                  <a href="javascript:void(0)" onclick="closeReport('dsr4f5mq5ysfn54o')">关闭</a>
                </td>
              </tr>

            </tbody></table>
        </div>
        <br />
        <br />
        <br />
        <br />

      </div>


    </div>
  )
};