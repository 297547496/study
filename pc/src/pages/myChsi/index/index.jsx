import { useState } from 'react';
import { Link } from 'react-router-dom'
import { ChsiNavTabs } from '../chsiNavTabs/index';
import { Content } from '../Content';

import "./index.css";
import "./archive-1.0.2.min.css";

export const ArchiveIndexPage = ({ }) => {
  const [activePage, setActivePage] = useState('index');

  return (
    <div className="container">
      {/* 顶部 */}
      <ChsiNavTabs activePage={activePage} setActivePage={setActivePage}></ChsiNavTabs>
      <div className="logon_centerBg">
        <div className="width1180">
          <Content activePage={activePage} setActivePage={setActivePage}></Content>
        </div>
      </div>

      {/* 底部 */}
      <div className="login_footer_my">
        Copyright © 2003-2023 <Link to={{ pathname: "/my.chsi/login" }}>学信网</Link>  All Rights Reserved

      </div>
    </div >
  )
}
