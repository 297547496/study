import logoIndex from "../../../static/img/logo-index.png";
import { Link } from 'react-router-dom'
import "./index.css";

export const ArchivePage = () => {
  return (
    <div className="container">
      <div className="child">

        {/* 顶部 */}
        <div className="width1100 dpf aic h80 jcsb">
          <div>
            <img src={logoIndex} className="mr20" />
          </div>
          <div className="dpf aic index-right" style={{width:'250px',color:'#fff'}}>
            <a className="a-box">学信网</a>
            <a className="a-box">帮助中心</a>
            <a className="a-box">联系我们</a>
          </div>
        </div>
        {/* 主体 */}
        <div className="mian-box dpf">
          <div className="width1100 pt20">
            <div className="index-title"></div>
            <div className="index-p1-img">
              <Link className="login-btn" to={{ pathname: "/my.chsi/login" }} style={{color:'#fff'}}>进入学信档案</Link>
            </div>
            <div className="part1-person"></div>
          </div>
        </div>
        {/* 菜单 */}
        <ul id="menu">
          <li><a href="" className="active">TOP</a></li>
          <li><a href="https://my.chsi.com.cn/archive/index.jsp#gdjy">高等教育信息</a></li>
          <li><a href="https://my.chsi.com.cn/archive/index.jsp#txjd">图像校对</a></li>
          <li><a href="https://my.chsi.com.cn/archive/index.jsp#zxyz">在线验证报告</a></li>
          <li><a href="https://my.chsi.com.cn/archive/index.jsp#xlycj">学历学位认证与成绩验证</a></li>
          <li><a href="https://my.chsi.com.cn/archive/index.jsp#hzsq">出国报告发送</a></li>
          <li><a href="https://my.chsi.com.cn/archive/index.jsp#dctp">调查/投票</a></li>
          <li><a href="https://xz.chsi.com.cn/survey/index.action">职业测评</a></li>
          <li><a href="https://my.chsi.com.cn/archive/index.jsp#ncss">就业</a></li>
          <li><a href="https://kl.chsi.com.cn/robot/index.action?system=my">学信机器人</a></li>
        </ul>
      </div>
    </div>
  )
}
