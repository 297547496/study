import React, { useEffect, useState } from 'react';
import logoIndex from "../../../static/img/logo-index2.png";

import wx from "../../../static/img/wx-icon.png";
import zfb from "../../../static/img/zfb-icon.png";
import { Link, useNavigate } from 'react-router-dom'
import './index.css'
import { getData } from '../../../utils/api';
export const ArchiveLoginPage = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const loginFun = async () => {
    if (!username || !password) {
      alert('请输入用户名和密码')
      return;
    }
    const res = await getData('/api/user/login', 'post', { account: username.target.value, password: password.target.value })
    if (res.data.code == 1) {
      console.log(res.data);
      window.localStorage.setItem('token', res.data.data.userinfo.token)
      window.open('/my.chsi/index')
    } else {
      alert(res.data.msg)
    }

  }

  return (
    <div className="container">
      {/* 顶部 */}
      <div className="login_header_my ">
        <div className="width1200 dpf jcsb aic h80">
          <img src={logoIndex} className="mr20" />
          <div className="dpf aic" style={{ color: '#fff' }}>
            <div>首页  &emsp;|&emsp;</div>
            <div>帮助中心 |&emsp;</div>
            <div>联系我们</div>
          </div>
        </div>
      </div>

      <div className="logon_centerBg">
        <div className="width1200">
          <div className="login-box-my mt30">
            <a href="//kl.chsi.com.cn/robot/index.action?system=account" target="_blank" className="ch-robot wap-hide"></a>
            <h2 className="txt_c fs30 lin50 c_3c">登录</h2>
            <p className="txt_c c_666 fs12">请使用<Link to={{ pathname: "/my.chsi/login" }}>学信网账号</Link> 进行登录 </p>
            <div className="cr_top_my">
              <div className="w100x ct_input">
                <span className="ct_img_yhm mr10"></span>
                <input className="input_text" value={username.value} onChange={(value) => setUsername(value)} type="text" style={{ outline: 'none' }} placeholder="手机号/邮箱" />
              </div>
              <div className="w100x ct_input">
                <span className="ct_img_ma mr10"></span>
                <input className="input_text" value={password.value} onChange={(value) => setPassword(value)} type="password" style={{ outline: 'none' }} placeholder="密码" />
              </div>
              <button className="btn_login" onClick={loginFun}>登录</button>
              <div className="dpf aic jcsb mb20 c_000">
                <p>找回密码</p>
                <p>注册</p>
              </div>
              <div className="dpf aic wx-container">
                <p className="dpf aic mr30">
                  <img src={wx} className="mr10" />
                  微信登录</p>
                <p className="dpf aic">
                  <img src={zfb} className="mr10" />
                  支付宝登录</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* 底部 */}
      <div className="login_footer_my">
        Copyright © 2003-2023 <Link to={{ pathname: "/my.chsi/login" }}>学信网</Link>  All Rights Reserved

      </div>
    </div>
  )
}
