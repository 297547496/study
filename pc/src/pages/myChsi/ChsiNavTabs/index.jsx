import './index.css'
import logoIndex from "../../../static/img/logo-index2.png";

export const ChsiNavTabs = ({ setActivePage }) => {
  return (
    <div className="login_header_my ">
      <div className="width1200 dpf aic h80">
        <img src={logoIndex} className="mr20" alt="" />
        <div className="homePage-nav" style={{ marginLeft: '28px' }}>
          <a onClick={() => {
            setActivePage('index')
          }} className="active" >首页</a>
        </div>
        <div id="nav-first" className="header-nav">
          <dl id="gdjy-nav">
            <dt>高等教育信息<span className="sanjiao"></span></dt>
            <dd style={{ display: 'none' }}><ul className="sub_nav_mian"><li>学籍信息</li>
              <li>图像校对</li>
              <li>学历信息</li>
              <li>学位信息</li>
              <li>考研信息</li>
            </ul></dd>
          </dl>
          <dl id="bab-nav">
            <dt>在线验证报告<span className="sanjiao"></span></dt>
            <dd style={{ display: 'none' }}><ul className="sub_nav_mian">
              <li>首页</li>
              <li>高等学籍</li>
              <li>高等学历</li>
              <li>学位</li>
              <li>协助申请</li>
            </ul></dd>
          </dl>
          <dl id="rzbg-nav">
            <dt>学历学位认证与成绩验证<span className="sanjiao"></span></dt>
            <dd style={{ display: 'none' }}><ul className="sub_nav_mian">
              <li>首页</li>
              <li>绑定报告</li>
            </ul></dd>
          </dl>
          <dl id="gjhz-nav">
            <dt>出国报告发送<span className="sanjiao"></span></dt>
            <dd style={{ display: 'none' }}><ul className="sub_nav_mian">
              <li>首页</li>
              <li>传输费用</li>
              <li>英文翻译</li>
            </ul></dd>
          </dl>
          <dl>
            <dt id="vote-nav">调查/投票<span className="sanjiao" style={{ left: '46px' }}></span></dt>
            <dd style={{ display: 'none' }}><ul className="sub_nav_mian">
              <li>首页</li>
              <li>跟踪调查</li>
              <li >就业跟踪</li>
              <li>专业推荐</li>
              <li>专业满意度</li>
              <li>院校满意度</li>
            </ul></dd>
          </dl>
          <dl>
            <dt id="xz-nav"><a href="http://xz.chsi.com.cn/survey/index.action" className="xz-link" target="_blank">职业测评</a></dt>
            <dd style={{ display: 'none' }}><ul className="sub_nav_mian">
              <li><a href="http://xz.chsi.com.cn/home.action" target="_blank">首页</a></li>
              <li><a href="http://xz.chsi.com.cn/speciality/index.action" target="_blank">专业洞察</a></li>
              <li><a href="http://xz.chsi.com.cn/survey/index.action" target="_blank">职业测评</a></li>
              <li><a href="http://xz.chsi.com.cn/occupation/index.action" target="_blank">职业探索</a></li>
              <li><a href="https://xz.chsi.com.cn/occucase/index.action" target="_blank">职业人物</a></li>
              <li><a href="https://xz.chsi.com.cn/jyzdk/index.action" target="_blank">就业指导课</a></li>
            </ul></dd>
          </dl>
          <dl>
            <dt id="ncss-nav"><a href="https://www.ncss.cn/" target="_blank" className="jy-link">就业</a></dt>
            <dd style={{ display: 'none' }}><ul className="sub_nav_mian">
              <li><a href="https://www.ncss.cn/" target="_blank">首页</a></li>
              <li><a href="https://job.ncss.cn/student/jobs/index.html" target="_blank">职位</a></li>
              <li><a href="https://job.ncss.cn/student/company/index.html" target="_blank">企业</a></li>
              <li><a href="https://job.ncss.cn/student/jobfair/index.html" target="_blank">招聘会</a></li>
            </ul></dd>
          </dl >
          <div className="header-nav-bg" style={{ display: 'none' }}>&nbsp;</div>
        </div >
        
      </div >
    </div >
  );
};