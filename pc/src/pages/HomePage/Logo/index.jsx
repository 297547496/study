
import LogoImg from "../../../static/img/logo.png";
import TitleImg from "../../../static/img/title.png";

export const Logo = () => {
  return (
    <div>
      {/* 顶部栏 */}
      <div className="width1180 dpf jcsb h109 bc_fff ">
        <div className="dpf aic ">
          <img src={LogoImg} className="mr20" />
          <div className="">
            <img src={TitleImg} style={{ width: '336px', height: '20px' }} />
            <div style={{color:'#889098',marginTop:'10px'}}>教育部学历查询网站、教育部高校招生阳光工程指定网站、全国硕士研究生招生报名和调剂指定网站</div>
          </div>
        </div>
        <div className="dpf aic" style={{color: '#889098',paddingTop:'50px'}}>
          <div>注册　|　</div>
          <div>登录　|　</div>
          <div>English</div>
        </div>

      </div>
    </div>
  )
}
