import { Footer } from './Footer'
import "./EducationQueryPage.css";

export const DegreeQueryPage = () => {
  return (
    <div className="content-block">
      <div className='educationQueryPage dpf'>
        <div className='r-main'>
          <div className="b-head">
            <h4 className="main_title">中国高等教育学位证书查询</h4>
            <div className="more">
              <a href="https://www.chsi.com.cn/help/cxfw.jsp" target="_blank">学历查询范围</a>
            </div>
          </div>
          <div className="b-body lscx-entry">
            <div className="m_f_div">
              <div className="brcx-icon"></div>
              <h3>本人查询</h3>
              <div className="description">
                <p>
                  注册学信网账号，登录学信档案，即可查询本人学历
                </p>
                <p className="mt20"><a className="colorBlue" href="/xlcx/brcxff.jsp" target="_blank">查询方法</a></p>
              </div>
              <div className="m_f_div_btn"><a href="https://my.chsi.com.cn/" target="_blank" className="btn_blue">查询</a></div>
            </div>
            <div className="m_f_div">
              <div className="lscx-icon"></div>
              <h3>零散查询</h3>
              <div className="description">
                <p>
                  输入证书编号及姓名进行查询
                </p>
              </div>
              <div className="m_f_div_btn"><a href="/xlcx/lscx/query.do" className="btn_blue">查询</a> </div>
            </div>
            <div className="m_f_div">
              <div className="qyecx-icon"></div>
              <h3>企业用户查询</h3>
              <div className="description">
                <p>
                  企业用户登录国家大学生就业服务平台进行查询
                </p>
              </div>
              <div className="m_f_div_btn"><a href="https://jy.chsi.com.cn/corpusr/index.do" target="_blank" className="btn_blue">查询</a></div>
            </div>
          </div>

          <div className="b-bottom">
            <div className="js_report_r">
              <div className="sub-description xl_js_r">
                <h3>中国高等教育学位在线验证报告</h3>
                <p>分为中文版和翻译件(英文)，有效期内免费在线验证，使用便捷，多重防伪。</p>
              </div>
              <div className="enter">
                <a href="/xlcx/bgys.jsp" target="_blank" className="colorBlue">报告介绍</a> <a href="/xlcx/rhsq.jsp" target="_blank" className="colorBlue">如何申请</a>
              </div>
            </div>
          </div>

        </div>
        <div className="r-side">

          <div className="m_r" id="rightH">
            <h3 className="r-head"><span className="txt">政策及常识</span></h3>
            <div className="m_cnt_s">
              <ul>
                <li><a href="/jyzx/202208/20220814/2210367104.html" target="_blank">中华人民共和国学位条例 </a></li>
                <li><a href="/jyzx/202208/20220814/2210367105.html" target="_blank">中华人民共和国学位条例暂行实施办法</a></li>
                <li><a href="/help/xwcx_lscx.jsp" target="_blank">常见问题</a></li>
              </ul>
            </div>
          </div>
          <div className="m_r" style={{paddingtop: '0'}}>
            <div className="devider"></div>
            <h3 className="r-head"><span className="txt">学位认证</span></h3>
            <div className="m_cnt_s">
              <ul>
                <li>
                  <a href="https://xwrz.chsi.com.cn/" target="_blank" onmousedown="gaTrackerOutboundLink(this, 'xwrz', 'click', 'fromxwcx');">学位认证报告申请及查询 &gt;</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div >
  );
};