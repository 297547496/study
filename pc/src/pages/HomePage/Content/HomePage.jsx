import { Carousel } from 'antd';
import { Footer } from './Footer';
import './index.css'

export const HomePage = () => {

  return (
    <div className="content-block">
      {/* 轮播 */}
      <div className='part01'>
      <div className='width1180'>
      <Carousel style={{padding: '20px 0 50px'}}>
        <img src="https://t2.chei.com.cn/news/img/2293107153.jpg" alt="" />
        <img src="https://t1.chei.com.cn/news/img/2293097840.jpg" alt="" />
        <img src="https://t2.chei.com.cn/news/img/2293086044.jpg" alt="" />
      </Carousel>
      </div>
      
      {/* 新闻模块 */}
      <div className='width1180'>
      <div style={{display: 'flex'}}>
      <div style={{display: 'flex',}} className='news-left'>
          <div>
            <div className='font20'>
              <a  className='color-red' target="_blank" href='https://yz.chsi.com.cn/kyzx/jybzc/202308/20230823/2293107421.html'>2024年教育专业学位硕士《教育综合考试大纲》正式公布</a>
            </div>
            <div>
            <a  target="_blank" href='https://yz.chsi.com.cn/kyzx/zt/lnfsx2023.shtml'>近五年考研分数线及趋势图（2019-2023） {'\u00A0'}| {'\u00A0'}</a>
              <a  target="_blank" href='https://www.ncss.cn/student/jobfair/fairdetails.html?fairId=YTwByZQmNyp8HWmcHCpYb1'> 专精特新中小企业2023届毕业生网上招聘活动</a>
            </div>
            <div>
              <a  target="_blank" href='https://www.chsi.com.cn/jyzx/202308/20230802/2293109545.html'>引导鼓励高校毕业生赴基层就业 {'\u00A0'}| {'\u00A0'}</a>
              <a  target="_blank" href='https://www.chsi.com.cn/jyzx/202308/20230816/2293106973.html'> 提供暖心服务 力促精准就业 {'\u00A0'}| {'\u00A0'}</a>
              <a  target="_blank" href='https://www.ncss.cn/ncss/zt/wxyjgl.shtml'>毕业生五险一金全攻略</a>
            </div>
            <div className='font20 colorBlue'>
              <a  className="colorBlue" target="_blank" href='https://gaokao.chsi.com.cn/z/gkbmfslq/lqjg.jsp'>高考录取日程及录取结果查询 {'\u00A0'}| {'\u00A0'}</a>
              <a  className="colorBlue" target="_blank" href='https://gaokao.chsi.com.cn/gkxx/zt/2023gkfzp.shtml'>高考防诈骗指南</a>
            </div>
            <div>
              <a  target="_blank" href='https://gaokao.chsi.com.cn/gkxx/zc/moe/202307/20230703/2293098248.html'>教育部暑期高校学生资助热线电话开通 {'\u00A0'}| {'\u00A0'}</a>
              <a  target="_blank" href='https://gaokao.chsi.com.cn/gkxx/ksbd/202308/20230811/2293110107.html'>准大学生你好：新的起点，你准备好了吗</a>
            </div><div>
              <a  target="_blank" href='https://www.chsi.com.cn/jyzx/202102/20210208/2028106580.html'>套号学历涉嫌违法 {'\u00A0'}| {'\u00A0'}</a>
              <a  target="_blank" href='https://www.chsi.com.cn/zhaopin/index.jsp'>学信网招聘  {'\u00A0'}| {'\u00A0'}</a>
              <a  target="_blank" href='https://www.chsi.com.cn/jyzx/xmt.shtml'>新媒体矩阵  {'\u00A0'}| {'\u00A0'}</a>
              <a  target="_blank" href='https://www.chsi.com.cn/news/'>【新闻】  {'\u00A0'}| {'\u00A0'}</a>
              <a  target="_blank" href='https://www.chsi.com.cn/zthz/'>专题汇总</a>
            </div>
          </div>
        </div>
          {/* 右侧教育 */}
        <div className='news-right news-bg'>
            <div className='new-r-top'>
              <span>
              <img style={{width: '84px', height:'21px'}} src="https://t1.chei.com.cn/chsi/assets/gb/pc/images/index-jyzx.svg" alt="" />
              </span>
              <a href="https://www.chsi.com.cn/news/" target='_parent'>更多</a>
            </div>
            <ul className='new-r-list'>
                <li>
                  <a href="https://www.chsi.com.cn/jyzx/202302/20230209/2256612160.html">关于做好2023年同等学力人员申请硕士学位外国语水...</a></li>
                <li><a href="https://www.chsi.com.cn/jyzx/202308/20230822/2293107345.html">广东工业大学：将实验室搬到乡村</a></li>
                <li><a href="https://www.chsi.com.cn/jyzx/202308/20230821/2293107278.html">把绿水青山变成“金山银山”——天津大学助力甘肃宕昌...</a></li>
                <li><a href="https://www.chsi.com.cn/jyzx/202308/20230821/2293107276.html">青岛理工大学温情帮扶汛情受灾学生</a></li>
                <li><a href="https://www.chsi.com.cn/jyzx/202308/20230818/2293107128.html">招聘指导一站通，离校就业出新招</a></li>
                <li><a href="https://www.chsi.com.cn/jyzx/202308/20230818/2293107126.html">扬州市职业大学：助力毕业生高质量充分就业</a></li>
              </ul>
        </div>
      </div>
      </div>
      
      {/*  快速入口模块 */}
      <div className='width1180'>
      <div className='ksrk'>
        <div className='ksrk-1'></div>
        <div className='ksrk-r'>
          <a href="https://xjxl2.chsi.com.cn/htgl/">学籍学历信息管理平台</a>
          <a href="https://sic.chsi.com.cn/">来华留学生学籍学历管理平台</a>
          <a href="https://xw.chsi.com.cn/">学位授予信息报送（备案）系统</a>
          <a href="https://tdxl.chsi.com.cn/tdxlsqxt/index.html">全国同等学力人员申请硕士学位管理工作信息平台</a>
          <a href="https://gxjx.chsi.com.cn/dsk/">全国万名优秀创新创业导师库</a>
          <a href="https://chsi.wanfangtech.net/">毕业论文查重</a>
          <a href="https://www.chsi.com.cn/cjdyz/index">电子成绩单验证</a>
          <a href="https://sfs.chsi.com.cn/">师范生管理信息系统</a>
          <a href="https://exwzs.chsi.com.cn/">第二学士学位招生信息平台</a>
          <a href="https://www.hsehome.com/student/welcome">安全家——职场安全早班车</a>
        </div>

      </div>
      </div>
      </div>
      
     
      {/* 卡片 */}
      <div className='index-row part02 width1180'>
        <div className='card-wrap'>
          <div className='card'>
            <div className='card-top'>
              <h5 className="i01">学籍学历学位</h5>
              高等教育学籍/学历/学位信息查询、验证、认证
            </div>
            <div className="card-bottom">
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">学籍查询</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">学历查询</a><br/>
                        <a href="/xwcx/index.jsp" target="_parent">学位查询</a>
                    </li>
                    <li>
                        <a href="/xlcx/bgcx.jsp" target="_parent">学籍验证</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">学历验证</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">学位验证</a>
                    </li>
                    <li>
                        <br/>
                        <a href="/xlrz/index.jsp" target="_parent" >学历认证</a><br/>
                        <a href="https://xwrz.chsi.com.cn/" target="_blank">学位认证</a>
                    </li>
              </ul>
            </div>
          </div>
          <div className='card'>
            <div className='card-top'>
              <h5 className="i02">出国教育背景信息服务</h5>
              为学生出国留学提供国内教育背景调查信息服务
            </div>
            <div className="card-bottom">
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">网上申请</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">报告查验</a><br/>
                    </li>
                    <li>
                        <a href="/xlcx/bgcx.jsp" target="_parent">进度查询</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">申请材料</a><br/>
                    </li>
              </ul>
            </div>
          </div>
          <div className='card'>
            <div className='card-top'>
              <h5 className="i03">学信档案</h5>
              高等教育学生信息档案
            </div>
            <div className="card-bottom">
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">学籍信息</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">学历信息</a><br/>
                    </li>
                    <li>
                        <a href="/xlcx/bgcx.jsp" target="_parent">学位信息</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">考研信息</a><br/>
                    </li>
                    <li className='my-li'>
                        <a href="/xlrz/index.jsp" target="_parent" >图像校对</a><br/>
                        <a href="https://xwrz.chsi.com.cn/" target="_blank" >出国报告发送</a>
                    </li>
              </ul>
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">申请学籍/学历/学位在线验证报告</a><br/>
                    </li>
              </ul>
            </div>
          </div>
          <div className='card'>
            <div className='card-top'>
              <h5 className="i04">阳光高考</h5>
              教育部高校招生阳光工程指定平台
            </div>
            <div className="card-bottom">
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">招生政策</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">名单公示</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">咨询周</a>
                    </li>
                    <li>
                        <a href="/xlcx/bgcx.jsp" target="_parent">招生章程</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">各地网站导航</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">志愿参考</a>
                    </li>
              </ul>
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">申请学籍/学历/学位在线验证报告</a><br/>
                    </li>
              </ul>
            </div>
          </div>
          <div className='card'>
            <div className='card-top'>
              <h5 className="i05">研招网</h5>
              全国硕士研究生报名和调剂指定网站
            </div>
            <div className="card-bottom">
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">硕士目录</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">博士目录</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">推免服务</a>
                    </li>
                    <li>
                        <a href="/xlcx/bgcx.jsp" target="_parent">硕士网报</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">博士网报</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">信息公开</a>
                    </li>
                    <li className='my-li'>
                        <a href="/xlcx/bgcx.jsp" target="_parent">网上确认</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">成绩查询</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">网上调剂</a>
                    </li>
              </ul>
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">申请学籍/学历/学位在线验证报告</a><br/>
                    </li>
              </ul>
            </div>
          </div>
          <div className='card'>
            <div className='card-top'>
              <h5 className="i06">内地高校面向港澳台招生</h5>
              内地高校面向港澳台招生信息网
            </div>
            <div className="card-bottom">
              <ul className="card-ul">
                  <li>
                      <a href="https://my.chsi.com.cn/" target="_blank">本科生招生</a><br/>
                  </li>
                  <li>
                      <a href="https://my.chsi.com.cn/" target="_blank">研究生招生</a><br/>
                  </li>
              </ul>
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">依据学测成绩招收台湾高中毕业生</a><br/>
                    </li>
              </ul>
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">面向港澳台招收研究生网上报名</a><br/>
                    </li>
              </ul>
            </div>
          </div>
          <div className='card'>
            <div className='card-top'>
              <h5 className="i07">国家大学生就业服务平台</h5>
              24365校园招聘服务
            </div>
            <div className="card-bottom">
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">职位信息</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">网上签约</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">宏志助航</a>
                    </li>
                    <li>
                        <a href="/xlcx/bgcx.jsp" target="_parent">专场招聘</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">实习岗位</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">创服网</a>

                    </li>
                    <li className='my-li'>
                    <a href="/xlcx/bgcx.jsp" target="_parent">24365校招</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">风险提示</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">指导教师网</a>
                    </li>
              </ul>
            </div>
          </div>
          <div className='card'>
            <div className='card-top'>
              <h5 className="i08">全国征兵网</h5>
              全国征兵报名唯一官方网站
            </div>
            <div className="card-bottom">
              <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">兵役登记</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">女兵报名</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">入伍宣传单</a>
                    </li>
                    <li>
                        <a href="/xlcx/bgcx.jsp" target="_parent">男兵报名</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">招收军士</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">征兵宣传片</a>
                    </li>
              </ul>
            </div>
          </div>
          <div className='card'>
            <div className='card-top'>
              <h5 className="9">学职平台</h5>
              全国大学生学业与职业发展平台
            </div>
            <div className="card-bottom">
            <ul className="card-ul">
                    <li>
                        <a href="https://my.chsi.com.cn/" target="_blank">专业洞察</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">职业探索</a><br/>
                        <a href="/xlcx/index.jsp" target="_parent">职业微视频</a>
                    </li>
                    <li>
                        <a href="/xlcx/bgcx.jsp" target="_parent">职业测评</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">职业人物</a><br/>
                        <a href="/xlcx/bgcx.jsp" target="_parent">就业指导课</a>
                    </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      {/*  */}
      <div className="index-row part03">
        <ul className="width1180 part03-ul">
            <li>
                <a href="https://bm.chsi.com.cn/yszp/stu/judge" target="_blank" className="sed-menu-a">
                    <div className="icon icon_01"></div>
                    <h5>在线考试系统</h5>
                    <p>在线笔试及视频考试</p>
                </a>
            </li>
            <li>
                <a href="https://bm.chsi.com.cn/ycms/stu/" target="_blank" className="sed-menu-a">
                    <div className="icon icon_02"></div>
                    <h5>远程面试系统</h5>
                    <p>高校招生远程面试</p>
                </a>
            </li>
            <li>
                <a href="https://chsi.wanfangtech.net/" target="_blank" className="sed-menu-a" >
                    <div className="icon icon_03"></div>
                    <h5>毕业生论文查重</h5>
                    <p>毕业论文相似性检测（本科、硕士、博士）</p>
                </a>
            </li>
            <li>
                <a href="/cjdyz/index" className="sed-menu-a">
                    <div className="icon icon_04"></div>
                    <h5>电子成绩单验证</h5>
                    <p>高校学生在校成绩单验证</p>
                </a>
            </li>
        </ul>
        <ul className="width1180 part03-ul">
            <li>
                <a href="https://jybzp.chsi.com.cn/home/index" target="_blank" className="sed-menu-a">
                    <div className="icon icon_05"></div>
                    <h5>教育部人才服务网</h5>
                    <p>教育部直属单位公开招聘平台</p>
                </a>
            </li>
            <li>
                <a href="https://exwzs.chsi.com.cn/" target="_blank" className="sed-menu-a">
                    <div className="icon icon_06"></div>
                    <h5>第二学士学位招生</h5>
                    <p>全国普通高校第二学士学位招生信息平台</p>
                </a>
            </li>
            <li className="more-link-li">
                <a href="javascript:;" target="_blank" className="sed-menu-a">
                    <div className="icon icon_07"></div>
                    <h5>导师人才库</h5>
                    <p>全国万名优秀创新创业导师人才库</p>
                </a>
                <div className="more_link">
                    <a href="https://gxjx.chsi.com.cn/dsk/publish/mentor/index.action" target="_blank">名单查询</a>　
                    <a href="https://gxjx.chsi.com.cn/dsk/zs/index.action" target="_blank">证书查询</a>
                </div>
            </li>
            <li className="more-link-li">
                <a href="javascript:;" target="_blank" className="sed-menu-a">
                    <div className="icon icon_08"></div>
                    <h5>“校企行”创业就业导师</h5>
                    <p>导师人才库-“校企行”创业就业导师</p>
                </a>
                <div className="more_link">
                    <a href="https://gxjx.chsi.com.cn/dsk/publish/mentoryq/index.action" target="_blank">名单查询</a>　
                    <a href="https://gxjx.chsi.com.cn/dsk/xqxzs/index.action" target="_blank">证书查询</a>
                </div>
            </li>
        </ul>
    </div>
    {/*  */}
    <div className="index-row index-media">
        <div className="width1180">
            <div className="index-media-top">
                <h3 className="row-tit">官方微博 · 微信</h3>
                <a href="/jyzx/xmt.shtml" target="_blank">更多</a>
            </div>
            <ul>
                <li>
                    <img src="https://t3.chei.com.cn/chsi/assets/gb/pc/images/chsi-ewm.jpg" alt="学信网微信公众号" title="学信网微信公众号" width="80" />
                    <p>学信网</p>
                </li>
                <li>
                    <img src="https://t2.chei.com.cn/chsi/assets/gb/pc/images/gk-ewm.jpg" alt="阳光高考订阅号" title="阳光高考订阅号" width="80"/>
                    <p>阳光高考</p>
                </li>
                <li>
                    <img src="https://t1.chei.com.cn/chsi/assets/gb/pc/images/yz-ewm.jpg" alt="研招网服务号" title="研招网服务号" width="80"/>
                    <p>研招网</p>
                </li>
                <li>
                    <img src="https://t3.chei.com.cn/chsi/assets/gb/pc/images/xzy-ewm.jpg" alt="国家大学生就业服务平台订阅号" title="国家大学生就业服务平台订阅号" width="80"/>
                    <p>国家大学生就业服务平台</p>
                </li>
                <li>
                    <img src="https://t4.chei.com.cn/chsi/assets/gb/pc/images/xz-ewm.jpg" alt="学职平台公众号" title="学职平台公众号" width="80"/>
                    <p>学职平台</p>
                </li>
            </ul>
        </div>
    </div>
    {/*  */}
    <div className="index-row index-other-link">
        <div className="width1180">
            <h3 className="row-tit">友情链接</h3>
            <ul style={{paddingTop: '18px'}}>
                <li className="link01"><a href="https://www.moe.gov.cn/" target="_blank" rel="noopener noreferrer"></a></li>
                <li className="link02"><a href="https://www.moe.gov.cn/s78/A15/" target="_blank" rel="noopener noreferrer"></a></li>
                <li className="link03"><a href="https://www.cedf.org.cn/" target="_blank" rel="noopener noreferrer"></a></li>
                <li className="link04"><a href="https://www.ctdf.org.cn/" target="_blank" rel="noopener noreferrer"></a></li>
                <li className="link05"><a href="https://www.xszz.edu.cn/" target="_blank" rel="noopener noreferrer"></a></li>
                <li className="link06"><a href="http://edu.people.com.cn/" target="_blank" rel="noopener noreferrer"></a></li>
                <li className="link07"><a href="https://www.hsehome.com/" target="_blank" rel="noopener noreferrer"></a></li>
                <li className="link08"><a href="https://portal.dsedj.gov.mo/webdsejspace/site/chinaenroll/home.html" target="_blank" rel="noopener noreferrer"></a></li>
            </ul>
        </div>
    </div>
    {/* 底部 */}
    <Footer />
  </div>
  );
};