import React, { useEffect, useState } from 'react';
import { Footer } from './Footer'
import "./EducationQueryPage.css";
import "./Verification.css";
import { getData } from '../../../utils/api';
export const Verification = ({ setActivePage }) => {
  const [username, setUsername] = useState('');

  useEffect(() => {
  }, []);



  const handleLogin = async () => {
    if (username.target.value == '') {
      return;
    }
    window.open('/Reports?id=' + username.target.value)
  }


  return (
    <div className="content-block">
      <div className='educationQueryPage dpf'>
        <div className='main'>
          <div className='ui-box'>
            <ul className="zxyz-left-menu ivu-menu ivu-menu-light ivu-menu-vertical" style={{ width: '240px' }}><a href="/xlcx/bgcx.jsp" target="_self" className="ivu-menu-item ivu-menu-item-active ivu-menu-item-selected">
              在线验证
            </a> <a href="/xlcx/bgys.jsp" target="_self" className="ivu-menu-item">
                验证报告简介
              </a> <a href="/xlcx/fwcs.jsp" target="_self" className="ivu-menu-item">
                防伪措施
              </a> <a href="/xlcx/yzzw.jsp" target="_self" className="ivu-menu-item">
                验证真伪
              </a> <a href="/xlcx/tdhyt.jsp" target="_self" className="ivu-menu-item">
                特点和用途
              </a> <a href="/xlcx/rhsq.jsp" target="_self" className="ivu-menu-item">
                如何申请
              </a> <a href="/xlcx/rhsy.jsp" target="_self" className="ivu-menu-item">
                如何使用
              </a> <a href="/xlcx/rhyq.jsp" target="_self" className="ivu-menu-item">
                延长验证有效期
              </a> <a href="/xlcx/tbsm.jsp" target="_self" className="ivu-menu-item">
                特别声明
              </a></ul> <div className="zxyz-zygz-box"><h2>重要告知</h2> <ul className="zygz-list"><li><a href="/zxyz/202305/20230517/2286956491.html" target="_blank" className="news-name">关于调整学籍/学历/学位在线验证报告翻译件和学位认证报告翻译件的说明</a> <div className="news-date">2023-05-18</div></li></ul></div></div>
        </div>
        <div className="m_s_r">
          <h4 className="main_title margin-top-17">学籍/学历/学位在线验证报告查询</h4>
          <div className="zxyz-tip-box">
            <h3>关于免费申请学籍、学历电子验证报告的温馨提示</h3>
            <p>为贯彻落实国务院常务会议精神，方便群众办事，减轻群众负担，自2018年1月1日起，学信网将对《教育部学籍在线验证报告》及《教育部学历证书电子注册备案表》提供免费申请服务。</p>
          </div>
          <div className="search-wrap">
            <div className="search-zxzybg"><form method="post" name="getXueLi" action="/xlcx/bg.do?vcode=">
              <div className="content-item-warp"><div className="label">报告中的在线验证码</div> <div className="value">


                <input type="text" value={username.value} autocomplete="off" className="input_text XXXXXXXX input_t_l" onChange={(value) => setUsername(value)} />
              </div> <div className="btn"><button type="button" onClick={handleLogin} className="btn_blue ivu-btn ivu-btn-primary">
                <span>查询</span></button></div></div></form> <div className="tips" style={{ marginBottom: '10px' }}>
                2019年3月15日起，新申请的《教育部学籍在线验证报告》及《教育部学历证书电子注册备案表》的在线验证码升级为16位。
              </div>
              <div className="tips" style={{ marginTop: '0px' }}>
                2023年1月16日起，启用新版《教育部学籍在线验证报告》及《教育部学历证书电子注册备案表》，点此查看<a href="/xlcx/ybdb.jsp" target="_blank" className="blue-btn">新旧版报告样式对比</a>。
              </div>
            </div>
            <div className="alert-zxzybg">
              <h5 className="sub_title">提供的验证</h5> <div className="alert-zxzybg-bg"><p>1.《教育部学籍在线验证报告》（含中文版和翻译件（英文））</p>
                <p>2.《教育部学历证书电子注册备案表》（含中文版和翻译件（英文））</p> <p>3.《中国高等教育学位在线验证报告》（含中文版和翻译件（英文））</p>
              </div></div></div>
          <div className="rzbg-link-wrap" style={{ paddingbottom: '30px' }}><a href="/xlrz/index.jsp" onmousedown="gaTrackerOutboundLink(this, 'xlrz', 'click', 'fromzxyz');" className="xlrzbg-link">国内学历认证报告查询 &gt;</a> <a href="https://xwrz.chsi.com.cn/" onmousedown="gaTrackerOutboundLink(this, 'xwrz', 'click', 'fromzxyz');" className="xlrzbg-link">国内学位认证报告查询 &gt;</a>
          </div>
        </div>
      </div>
      <Footer />
    </div >
  );
};