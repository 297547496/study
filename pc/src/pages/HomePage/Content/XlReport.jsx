import React, { useEffect, useState } from 'react';
import { getData } from '../../../utils/api';
import { Footer } from './Footer'
import  Qrimg  from '../../../static/img/d53147c20q824284625cf09361070f261077685171.jpg'
import "./EducationQueryPage.css";
import "./Verification.css";
import "./report.css";
import { NavTabs } from '../NavTabs';
import { Logo } from '../Logo';
import { NAV_TABS } from '../NavTabs/consts'
export const XlReport = () => {
  const [activePage, setActivePage] = useState('home');
  useEffect(() => {

    console.log(window.location.search.split('?id=')[1]);
    if (window.location.search.split('?id=')[1]) {
      getYanzeng()
    } else {
      getList()

    }

  }, []);
  const [list, setList] = useState({})

  const getList = async () => {
    const res = await getData('/api/user/zhengshu', 'post')
    if (res.data.code == 1) {
      setList(res.data.data[0]);
    }
  }

  const getYanzeng = async () => {
    const res = await getData('/api/user/yanzheng', 'post', { online_code: window.location.search.split('?id=')[1] })
    if (res.data.code == 1) {
      setList(res.data.data[0]);
    }
  }

  // getList()



  return (
    <div style={{ background: '#fff' }}>
      <Logo></Logo>
      {/* <NavTabs setActivePage={setActivePage}></NavTabs> */}
      <nav className="nav">
        <ul>
          {NAV_TABS.map(item => {
            const { url, label, key, target } = item;
            return (
              <li onClick={() => {
                window.location.href = '/index'
                return
              }}>{label}</li>
            )
          })}
        </ul>
      </nav>
      <div className="content-block ">

        <div className='width1180x dpf'>
          <div>
            <ul className="zxyz-left-menu ivu-menu ivu-menu-light ivu-menu-vertical" style={{ width: '240px' }}>
              <a href="/xlcx/bgcx.jsp" target="_self" className="ivu-menu-item ivu-menu-item-active ivu-menu-item-selected">
                在线验证
              </a> <a href="/xlcx/bgys.jsp" target="_self" className="ivu-menu-item">
                验证报告简介
              </a> <a href="/xlcx/fwcs.jsp" target="_self" className="ivu-menu-item">
                防伪措施
              </a> <a href="/xlcx/yzzw.jsp" target="_self" className="ivu-menu-item">
                验证真伪
              </a> <a href="/xlcx/tdhyt.jsp" target="_self" className="ivu-menu-item">
                特点和用途
              </a> <a href="/xlcx/rhsq.jsp" target="_self" className="ivu-menu-item">
                如何申请
              </a> <a href="/xlcx/rhsy.jsp" target="_self" className="ivu-menu-item">
                如何使用
              </a> <a href="/xlcx/rhyq.jsp" target="_self" className="ivu-menu-item">
                延长验证有效期
              </a> <a href="/xlcx/tbsm.jsp" target="_self" className="ivu-menu-item">
                特别声明
              </a></ul>
            <div className="zxyz-zygz-box"><h2>重要告知</h2>
              <ul className="zygz-list">
                <li><a href="/zxyz/202305/20230517/2286956491.html" target="_blank" className="news-name">关于调整学籍/学历/学位在线验证报告翻译件和学位认证报告翻译件的说明</a>
                  <div className="news-date">2023-05-18</div>
                </li>
              </ul>
            </div>
          </div>
          <div className='m_s_r'>
            <br />
            <div className='dpf aic jcsb'>
              <div className='main_title'>学籍/学历/学位在线验证报告查询</div>
              <div className='dpf aic'>
                <div className="chooseLan zxyzChooseLan">
                  报告语种
                  &emsp;
                  <a href="/xlcx/bg.do?vcode=ADSNVL3FFJL89321&amp;lang=CN&amp;srcid=archive" className="current">中文</a>
                  <a href="#" id="openerHref">英文</a> <div id="noenreport" title="申请英文版" style={{ display: 'none' }}>
                    <span className="noenreportArrow"></span> <a href="#" title="关闭" className="newLayerClose"></a>
                    该报告尚无对应的翻译件（英文），您可登录<a href="https://my.chsi.com.cn/archive/index.jsp#zxyz" target="_blank" style={{ color: 'rgb(0, 102, 204)', marginLeft: '0px' }}>学信档案</a>进行申请。
                  </div></div>

                <div className="printBG clearfix">
                  <a href="javascript:;" title="打印" className="iconClick iconPrint">&nbsp;</a>
                  <a
                    href="/report/xueji/download.do?vcode=ADSNVL3FFJL89321&amp;rid=41616743524465210819599601280280&amp;ln=cn" title="下载" className="iconClick iconDownload">&nbsp;</a>
                  <a id="showEmail" href="#" title="发送" className="iconClick iconEmail">&nbsp;</a>
                  <a id="openxxewm" href="#" title="学信二维码" className="iconClick iconXxewm">&nbsp;</a>
                </div>
              </div>
            </div>



            <div id="resultTable"><div className="zxyz-res xj-zxyz-res">
              <h4>教育部学历在线验证报告</h4>
              <div className="update-time">更新日期：2023年08月27日</div>
              <div className="report-info"><div className="r-zp-wrap">
                <img src={'http://154.83.14.37:81/' + list.luqu_avatar} alt="" className="report-info-zp" />
              </div>
                <div className="report-info-item">
                  <div className="label">姓名</div>
                  <div className="value">{list.nickname}</div>
                </div> <div className="report-info-item">
                  <div className="label">性别</div>
                  <div className="value">{list.gender == 0 ? '女' : '男'}</div>
                </div>
                <div className="report-info-item">
                  <div className="label">出生日期</div>
                  <div className="value">{list.birthday}</div>
                </div>
                <div className="report-info-item"><div className="label">入学日期</div>
                  <div className="value long">{list.enrollment_time}</div></div>
                <div className="report-info-item"><div className="label">毕（结）业日期</div>
                  <div className="value long">{list.enrollment_time}</div></div>
                <div className="report-info-item">
                  <div className="label">学校名称</div>
                  <div className="value long">{list.school_name}</div>
                </div>
                <div className="report-info-item">
                  <div className="label">专业</div>
                  <div className="value long">{list.zhuanye}</div>
                </div>
                <div className="report-info-item">
                  <div className="label">学制</div>
                  <div className="value long">{list.xuezhi}</div>
                </div>
                <div className="report-info-item">
                  <div className="label">层次</div>
                  <div className="value long">{list.xueli_level}</div>
                </div>
                <div className="report-info-item"><div className="label">学历类别</div>
                  <div className="value long">{list.leibie}</div></div>
                <div className="report-info-item"><div className="label">学习形式</div>
                  <div className="value long">{list.xingshi}</div></div>
                <div className="report-info-item"><div className="label">毕（结）业</div>
                  <div className="value long">{list.is_biye = 0 ? '结业' : '毕业'}</div></div>
                <div className="report-info-item"><div className="label">校（院）长姓名</div>
                  <div className="value long">{list.rector}</div></div>
              </div> <div className="zxyz-res-bottom"><div className="zxyz-res-code">
                <div className="left">
                  <img src={Qrimg} width="90" className="bg_ewm" /></div>
                <div className="right"><div className="r_top"><span className="text">在线验证码</span> <span className="yzm">{list.online_code}</span>
                </div> <span className="xh">①</span>验证报告在线查验网址：https://www.chsi.com.cn/xlcx/bgcx.jsp<br /> <span className="xh">②</span>使用学信网App扫描二维码验证
                </div></div> <div className="zxyz-res-tip"><h3 className="zxyz-res-tip-title">注意事项：</h3> <div className="zxyz-res-tip-content">
                  1、《学籍在线验证报告》是教育部学籍电子注册备案的查询结果。<br />
                  2、报告内容如有修改，请以最新在线验证的内容为准。<br />
                  3、未经学籍信息权属人同意，不得将报告用于违背权属人意愿之用途。<br />
                  4、报告在线验证有效期由报告权属人设置（1~6个月），其在报告验证到期前可再次延长验证有效期。
                </div></div></div> <div className="zxyz-res-bot"><img src="https://t1.chei.com.cn/chsi/images/logo.png" height="32" /></div></div></div>



























          </div>

        </div>

        <Footer />
      </div >
    </div>

  );
};