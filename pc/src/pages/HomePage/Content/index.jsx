import { HomePage } from './HomePage'
import { EducationQueryPage } from './EducationQueryPage';
import { DegreeQueryPage } from './DegreeQueryPage'
import { Verification } from './Verification'
import { Report } from './Report'

export const Content = ({ activePage, setActivePage }) => {
  switch (activePage) {
    case 'home':
      return <HomePage />;
    case 'educationQuery':
      return <EducationQueryPage />;
    case 'degreeQuery':
      return <DegreeQueryPage />;
    case 'verification':
      return <Verification />;
    case 'verification':
      return <Verification setActivePage={setActivePage} />;
    case 'report':
      return <Report setActivePage={setActivePage} />;
    default:
      return null;
  }
};