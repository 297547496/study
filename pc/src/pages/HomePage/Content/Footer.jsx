export const Footer = () => {
    return (
        <div class="foot-wrap" id="footer">
            <div class="footer-nav">
                <a href="https://chesicc.chsi.com.cn/zxgw/zxjs/201604/20160418/1529506207.html" target="_blank">中心简介</a>
                <a href="/about/about_site.shtml" target="_blank">网站简介</a>
                <a href="/about/copyright.shtml" target="_blank">版权声明</a>
                <a href="/ad/index.shtml" target="_blank">网站宣传</a>
                <a href="/about/contact.shtml" target="_blank">联系我们</a>
                <a href="/zhaopin/index.jsp" target="_blank">招聘信息</a>
                <a href="/help" target="_blank">帮助中心</a>
                <a href="/z/tenyears/index.jsp" target="_blank">学信十周年</a>
                <a href="/about/ct16.shtml" target="_blank">大事记</a>
            </div>
            <div class="footer width1180 clearfix">
                <div class="border-r" id="footerListFirst">
                    客服热线：010-67410388<br/>
                        <div id="kfEmail">客服邮箱：kefu#chsi.com.cn（将#替换为@）</div>
                </div>
                <div class="border-r">
                    主办单位：<a href="https://chesicc.chsi.com.cn/" target="_blank">教育部学生服务与素质发展中心</a><br/>
                        Copyright © 2003-2023 <a href="https://www.chsi.com.cn/" target="_blank">学信网</a> All Rights Reserved
                </div>
                <div class="footer-list">
                    <a href="https://beian.miit.gov.cn" target="_blank">京ICP备19004913号-1</a><br/>
                        <a href="https://www.beian.gov.cn/portal/registerSystemInfo?recordcode=11010202009747" target="_blank"><img src="https://t3.chei.com.cn/chsi/images/jgwab.png"/> 京公网安备11010202009747号</a>
                </div>
            </div>
        </div>
    )
}