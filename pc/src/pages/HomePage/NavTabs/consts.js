export const NAV_TABS = [
	{
		label: "首页",
		key: "home",
	},
	{
		label: "学籍查询",
		key: "studentStatusQuery",
		url: "/my.chsi/archive",
	},
	{
		label: "学历查询",
		key: "educationQuery",
	},
	{
		label: "学位查询",
		key: "degreeQuery",
	},
	{
		label: "在线验证",
		key: "verification",
	},
	{
		label: "出国教育背景信息服务",
		key: "overseasEducation",
    url: 'https://www.chsi.com.cn/xlrz/index2.jsp',
    target: '_self'
	},
	{
		label: "图像校对",
		key: "imageProofreading",
		url: "/my.chsi/archive",
	},
	{
		label: "学信档案",
		key: "informationArchives",
		url: "/my.chsi/archive",
	},
	{
		label: "高考",
		key: "collegeEntranceExamination",
		url: "https://gaokao.chsi.com.cn/",
	},
	{
		label: "研招",
		key: "researchRecruitment",
		url: "https://yz.chsi.com.cn/",
	},
	{
		label: "港澳台招生",
		key: "recruitStudents",
		url: "https://www.gatzs.com.cn/",
	},
	{
		label: "征兵",
		key: "conscription",
		url: "https://www.gfbzb.gov.cn/",
	},
	{
		label: "就业",
		key: "report",
		url: "https://www.ncss.cn/",
	},
	{
		label: "学职平台",
		key: "studentCareerPlatform",
		url: "https://xz.chsi.com.cn/home.action",
	},
];
