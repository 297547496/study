import './index.css'
import {NAV_TABS} from './consts'

export const NavTabs = ({ setActivePage }) => {
  return (
    <nav className="nav">
      <ul>
      {NAV_TABS.map(item => {
        const {url, label, key, target} = item;
        return(
        <li onClick={() => {
          if(url) {
            window.open(url, target || '_blank');
            return
          }
          setActivePage(key)
        }}>{label}</li>
        )
      })}
      </ul>
    </nav>
  );
};