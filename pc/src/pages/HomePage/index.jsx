import { useEffect, useState } from 'react';
import { NavTabs } from './NavTabs';
import { Content } from './Content';
import { Logo } from './Logo';

export const HomePage = () => {
  const [activePage, setActivePage] = useState('home');
  useEffect(() => {
    if (window.navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)) {
      window.location = "http://h5.cxl1.cn/";
    } else {
      // window.location = "http://www.cxl1.cn/";
    }
}, []);



  return (
    <div className='bc_fff'>
      <Logo />
      <NavTabs setActivePage={setActivePage} />
      <Content activePage={activePage} setActivePage={setActivePage} />
    </div>
  );
}
